# System-State-Framework Manager
**ssf-mgr** is a small userspace application complementing the system state framework. It is possible to 
start it with the option to monitor the device start-up and mark the system as up when everything is booted.
Additionally a watchdog feature is per default active which can be disabled via command line option.

## Features
Call the ssf-mgr with the option ``-f`` to see the possiblity:
```bash
ssf-mgr -h
```

## Build
### Unit Tests
Building the unit tests on command line is pretty easy:
```bash
# change to the build directory:
cd build
# load environment:
. env.utest
# build and run tests:
make -f Makefile.utest.mk
```

Build on 
