#**********************************************************************************************
# default environment (if not prepared before)
#**********************************************************************************************
#Set this to @ to keep the makefile quiet
SILENCE 		?= @

WORKSPACE_DIR	?= $(HOME)/workspace
BSP_PATH		?= $(WORKSPACE_DIR)/bsp
SYSROOT_BASEDIR		?= sysroots
SYSROOT			?= $(BSP_PATH)/$(SYSROOT_BASEDIR)/x86_64-netmodule-linux
CPPUTEST_HOME		?= $(WORKSPACE_DIR)/cpputest
UNITTEST_OUTPUT_XML	?= 0
UNITTEST_OUTPUT_COLORED	?= 0
USE_INITSYS_DUMMY	?= 1

#**********************************************************************************************
# project specific configuration
#**********************************************************************************************
LIBNAME		 = ssf-mgr
BUILD_TARGET	?= host
BUILD_CONFIG	?= test
REPO_PATH	?= $(shell git rev-parse --show-toplevel)
PROJECT_PATH	?= $(REPO_PATH)
BUILD_PATH	?= $(PROJECT_PATH)/build
OUT_DIR		?= $(BUILD_PATH)/$(BUILD_CONFIG)/$(BUILD_TARGET)
TEST_DIR	?= $(PROJECT_PATH)/tests

SRC_DIRS	 = $(shell find $(PROJECT_PATH)/src -name '*.c' -printf '%h\n' | sort -u)
SRC_DIRS	+= $(shell find $(PROJECT_PATH)/src -name '*.cpp' -printf '%h\n' | sort -u)
TEST_SRC_DIRS	 = $(shell find $(TEST_DIR) -name '*.cpp' -printf '%h\n' | sort -u)
MOCKS_SRC_DIRS	 = $(shell find $(TEST_DIR)/mocks -name '*.c' -printf '%h\n' 2>/dev/null | sort -u)
MOCKS_SRC_DIRS	+= $(shell find $(TEST_DIR)/mocks -name '*.cpp' -printf '%h\n' 2>/dev/null | sort -u)

INCLUDE_DIRS	+= $(CPPUTEST_HOME)/include \
		   $(CPPUTEST_HOME)/Platforms/Gcc \
		   $(PROJECT_PATH)/include \
		   $(PROJECT_PATH)/src \
		   $(TEST_DIR) \
		   $(TEST_DIR)/mocks \
		   $(SYSROOT)/usr/include

LIBS_TEST	+= nmapp-test \
		   CppUTest \
		   CppUTestExt

LIB_DIRS_TEST	+= $(SYSROOT)/usr/lib \
		   $(CPPUTEST_HOME)/lib

#**********************************************************************************************
# cpputest specific configuration
#**********************************************************************************************
COMPONENT_NAME		 = $(LIBNAME)
CPPUTEST_OBJS_DIR	 = $(OUT_DIR)/objs
CPPUTEST_LIB_DIR	 = $(OUT_DIR)/lib
CPPUTEST_LDFLAGS	+= $(foreach librarydir,$(LIB_DIRS_TEST),-L$(librarydir))
CPPUTEST_LDFLAGS	+= $(foreach library,$(LIBS_TEST),-l$(library)) 

ifeq ($(shell $(CC) -v 2>&1 | grep -c "clang"), 1)
	CPPUTEST_WARNINGFLAGS += -Wno-unknown-warning-option -Wno-covered-switch-default -Wno-reserved-id-macro
	CPPUTEST_WARNINGFLAGS += -Wno-keyword-macro -Wno-documentation -Wno-missing-noreturn
endif
CPPUTEST_WARNINGFLAGS	+= -Wall -Werror -Wfatal-errors -Wconversion -Wswitch-enum -Wswitch-default
CPPUTEST_WARNINGFLAGS	+= -Wno-format-nonliteral -Wno-sign-conversion -Wno-pedantic -Wno-shadow
CPPUTEST_WARNINGFLAGS	+= -Wno-missing-field-initializers -Wno-unused-parameter

CPPUTEST_CFLAGS		+= -D__TEST -D__PLATFORM_LINUX_POSIX
CPPUTEST_CFLAGS		+= -Wall -pedantic -Wno-missing-prototypes -Wno-strict-prototypes

CPPUTEST_CPPFLAGS	+= -D__TEST -D__PLATFORM_LINUX_POSIX
CPPUTEST_CXXFLAGS	+= -Wno-c++14-compat -Wno-c++98-compat-pedantic -Wno-c++98-compat

ifeq ($(USE_INITSYS_DUMMY), 1)
	CPPUTEST_CFLAGS		+= -D__USE_INIT_SYS_DUMMY
	CPPUTEST_CPPFLAGS	+= -D__USE_INIT_SYS_DUMMY
endif


CPP_PLATFORM					= Gcc
CPPUTEST_ENABLE_DEBUG			= Y
CPPUTEST_USE_MEM_LEAK_DETECTION	= Y
CPPUTEST_USE_EXTENSIONS			= Y
CPPUTEST_USE_STD_C_LIB			= Y
CPPUTEST_USE_STD_CPP_LIB		= Y
CPPUTEST_USE_LONG_LONG			= N

#create xml output in junit format
ifeq ($(UNITTEST_OUTPUT_XML), 1)
	CPPUTEST_EXE_FLAGS	+= -ojunit
endif
#colorize the test output
ifeq ($(UNITTEST_OUTPUT_COLORED), 1)
	CPPUTEST_EXE_FLAGS	+= -c
endif

#**********************************************************************************************
# setup unit test environment
#**********************************************************************************************

CC_VERSION_OUTPUT ="$(shell $(CXX) -v 2>&1)"

CPPUTEST_LIB_DIR = lib
CPPUTEST_LIB_LINK_DIR ?= $(CPPUTEST_HOME)/lib

# Without the C library, we'll need to disable the C++ library and ...
ifeq ($(CPPUTEST_USE_STD_C_LIB), N)
	CPPUTEST_USE_STD_CPP_LIB = N
	CPPUTEST_USE_MEM_LEAK_DETECTION = N
	CPPUTEST_CPPFLAGS += -DCPPUTEST_STD_C_LIB_DISABLED
	CPPUTEST_CPPFLAGS += -nostdinc
endif

ifeq ($(CPPUTEST_USE_MEM_LEAK_DETECTION), N)
	CPPUTEST_CPPFLAGS += -DCPPUTEST_MEM_LEAK_DETECTION_DISABLED
else
	ifndef CPPUTEST_MEMLEAK_DETECTOR_NEW_MACRO_FILE
		CPPUTEST_MEMLEAK_DETECTOR_NEW_MACRO_FILE = -include $(CPPUTEST_HOME)/include/CppUTest/MemoryLeakDetectorNewMacros.h
	endif
	ifndef CPPUTEST_MEMLEAK_DETECTOR_MALLOC_MACRO_FILE
		CPPUTEST_MEMLEAK_DETECTOR_MALLOC_MACRO_FILE = -include $(CPPUTEST_HOME)/include/CppUTest/MemoryLeakDetectorMallocMacros.h
	endif
endif

ifeq ($(CPPUTEST_USE_LONG_LONG), Y)
	CPPUTEST_CPPFLAGS += -DCPPUTEST_USE_LONG_LONG
endif

ifeq ($(CPPUTEST_ENABLE_DEBUG), Y)
	CPPUTEST_CXXFLAGS += -g
	CPPUTEST_CFLAGS += -g
	CPPUTEST_LDFLAGS += -g
endif

ifeq ($(CPPUTEST_USE_STD_CPP_LIB), N)
	CPPUTEST_CPPFLAGS += -DCPPUTEST_STD_CPP_LIB_DISABLED
ifeq ($(CPPUTEST_USE_STD_C_LIB), Y)
	CPPUTEST_CXXFLAGS += -nostdinc++
endif
endif

CPPUTEST_CXXFLAGS += $(CPPUTEST_WARNINGFLAGS) $(CPPUTEST_CXX_WARNINGFLAGS)
CPPUTEST_CPPFLAGS += $(CPPUTEST_WARNINGFLAGS)
CPPUTEST_CXXFLAGS += $(CPPUTEST_MEMLEAK_DETECTOR_NEW_MACRO_FILE)
CPPUTEST_CPPFLAGS += $(CPPUTEST_MEMLEAK_DETECTOR_MALLOC_MACRO_FILE)
CPPUTEST_CFLAGS += $(CPPUTEST_C_WARNINGFLAGS)

# Link with CppUTest lib
CPPUTEST_LIB = $(CPPUTEST_LIB_LINK_DIR)/libCppUTest.a

ifeq ($(CPPUTEST_USE_EXTENSIONS), Y)
CPPUTEST_LIB += $(CPPUTEST_LIB_LINK_DIR)/libCppUTestExt.a
endif

ifdef CPPUTEST_STATIC_REALTIME
	LD_LIBRARIES += -lrt
endif

TARGET_LIB = \
    $(CPPUTEST_LIB_DIR)/lib$(COMPONENT_NAME).a

TEST_TARGET = $(COMPONENT_NAME)_tests

#**********************************************************************************************
# build and run unit tests
#**********************************************************************************************
#Helper Functions
get_src_from_dir  = $(wildcard $1/*.cpp) $(wildcard $1/*.cc) $(wildcard $1/*.c)
get_dirs_from_dirspec  = $(wildcard $1)
get_src_from_dir_list = $(foreach dir, $1, $(call get_src_from_dir,$(dir)))
__src_to = $(subst .c,$1, $(subst .cc,$1, $(subst .cpp,$1,$(if $(CPPUTEST_USE_VPATH),$(notdir $2),$2))))
src_to = $(addprefix $(CPPUTEST_OBJS_DIR)/,$(call __src_to,$1,$2))
src_to_o = $(call src_to,.o,$1)
src_to_d = $(call src_to,.d,$1)
src_to_gcda = $(call src_to,.gcda,$1)
src_to_gcno = $(call src_to,.gcno,$1)
time = $(shell date +%s)
delta_t = $(eval minus, $1, $2)
debug_print_list = $(foreach word,$1,echo "  $(word)";) echo;

#Derived
STUFF_TO_CLEAN += $(TEST_TARGET) $(TEST_TARGET).exe $(TARGET_LIB) $(TARGET_MAP)

SRC += $(call get_src_from_dir_list, $(SRC_DIRS)) $(SRC_FILES)
OBJ = $(call src_to_o,$(SRC))

STUFF_TO_CLEAN += $(OBJ)

TEST_SRC += $(call get_src_from_dir_list, $(TEST_SRC_DIRS)) $(TEST_SRC_FILES)
TEST_OBJS = $(call src_to_o,$(TEST_SRC))
STUFF_TO_CLEAN += $(TEST_OBJS)


MOCKS_SRC += $(call get_src_from_dir_list, $(MOCKS_SRC_DIRS))
MOCKS_OBJS = $(call src_to_o,$(MOCKS_SRC))
STUFF_TO_CLEAN += $(MOCKS_OBJS)

ALL_SRC = $(SRC) $(TEST_SRC) $(MOCKS_SRC)

RUN_TEST_TARGET = $(SILENCE) echo "Running $(TEST_TARGET)"; ./$(TEST_TARGET) $(CPPUTEST_EXE_FLAGS)

INCLUDES_DIRS_EXPANDED = $(call get_dirs_from_dirspec, $(INCLUDE_DIRS))
INCLUDES += $(foreach dir, $(INCLUDES_DIRS_EXPANDED), -I$(dir))
MOCK_DIRS_EXPANDED = $(call get_dirs_from_dirspec, $(MOCKS_SRC_DIRS))
INCLUDES += $(foreach dir, $(MOCK_DIRS_EXPANDED), -I$(dir))

CPPUTEST_CPPFLAGS +=  $(INCLUDES)

DEP_FILES = $(call src_to_d, $(ALL_SRC))
STUFF_TO_CLEAN += $(DEP_FILES) $(PRODUCTION_CODE_START) $(PRODUCTION_CODE_END)
STUFF_TO_CLEAN += $(STDLIB_CODE_START) $(MAP_FILE) cpputest_*.xml junit_run_output

# We'll use the CPPUTEST_CFLAGS etc so that you can override AND add to the CppUTest flags
CFLAGS = $(CPPUTEST_CFLAGS) $(CPPUTEST_ADDITIONAL_CFLAGS)
CPPFLAGS = $(CPPUTEST_CPPFLAGS) $(CPPUTEST_ADDITIONAL_CPPFLAGS)
CXXFLAGS = $(CPPUTEST_CXXFLAGS) $(CPPUTEST_ADDITIONAL_CXXFLAGS)
LDFLAGS = $(CPPUTEST_LDFLAGS) $(CPPUTEST_ADDITIONAL_LDFLAGS)

# Don't consider creating the archive a warning condition that does STDERR output
ARFLAGS := $(ARFLAGS)c

DEP_FLAGS=-MMD -MP

# Some macros for programs to be overridden. For some reason, these are not in Make defaults
RANLIB = ranlib


# Targets

.PHONY: all
all: start $(TEST_TARGET)
	$(RUN_TEST_TARGET)

.PHONY: start
start: $(TEST_TARGET)
	$(SILENCE)START_TIME=$(call time)

.PHONY: all_no_tests
all_no_tests: $(TEST_TARGET)

.PHONY: flags
flags:
	@echo
	@echo "OS Linux"
	@echo "Compile C and C++ source with CPPFLAGS:"
	@$(call debug_print_list,$(CPPFLAGS))
	@echo "Compile C++ source with CXXFLAGS:"
	@$(call debug_print_list,$(CXXFLAGS))
	@echo "Compile C source with CFLAGS:"
	@$(call debug_print_list,$(CFLAGS))
	@echo "Link with LDFLAGS:"
	@$(call debug_print_list,$(LDFLAGS))
	@echo "Link with LD_LIBRARIES:"
	@$(call debug_print_list,$(LD_LIBRARIES))
	@echo "Create libraries with ARFLAGS:"
	@$(call debug_print_list,$(ARFLAGS))

TEST_DEPS = $(TEST_OBJS) $(MOCKS_OBJS) $(PRODUCTION_CODE_START) $(TARGET_LIB) $(USER_LIBS) $(PRODUCTION_CODE_END) $(CPPUTEST_LIB) $(STDLIB_CODE_START)
test-deps: $(TEST_DEPS)

$(TEST_TARGET): $(TEST_DEPS)
	@echo Linking $@
	$(SILENCE)$(CXX) -o $@ $^ $(LD_LIBRARIES) $(LDFLAGS)

$(TARGET_LIB): $(OBJ)
	@echo Building archive $@
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(AR) $(ARFLAGS) $@ $^
	$(SILENCE)$(RANLIB) $@

test: $(TEST_TARGET)
	$(RUN_TEST_TARGET) | tee $(TEST_OUTPUT)

vtest: $(TEST_TARGET)
	$(RUN_TEST_TARGET) -v  | tee $(TEST_OUTPUT)

$(CPPUTEST_OBJS_DIR)/%.o: %.cc
	@echo compiling $(notdir $<)
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(COMPILE.cpp) $(DEP_FLAGS) $(OUTPUT_OPTION) $<

$(CPPUTEST_OBJS_DIR)/%.o: %.cpp
	@echo compiling $(notdir $<)
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(COMPILE.cpp) $(DEP_FLAGS) $(OUTPUT_OPTION) $<

$(CPPUTEST_OBJS_DIR)/%.o: %.c
	@echo compiling $(notdir $<)
	$(SILENCE)mkdir -p $(dir $@)
	$(SILENCE)$(COMPILE.c) $(DEP_FLAGS) $(OUTPUT_OPTION) $<

ifneq "$(MAKECMDGOALS)" "clean"
-include $(DEP_FILES)
endif

.PHONY: clean
clean:
	@echo Making clean
	$(SILENCE)$(RM) $(STUFF_TO_CLEAN)
	$(SILENCE)rm -rf $(CPPUTEST_OBJS_DIR) $(CPPUTEST_LIB_DIR)

.PHONY: format
format:
	$(CPPUTEST_HOME)/scripts/reformat.sh $(PROJECT_HOME_DIR)

.PHONY: debug
debug:
	@echo
	@echo "Target Source files:"
	@$(call debug_print_list,$(SRC))
	@echo "Target Object files:"
	@$(call debug_print_list,$(OBJ))
	@echo "Test Source files:"
	@$(call debug_print_list,$(TEST_SRC))
	@echo "Test Object files:"
	@$(call debug_print_list,$(TEST_OBJS))
	@echo "Mock Source files:"
	@$(call debug_print_list,$(MOCKS_SRC))
	@echo "Mock Object files:"
	@$(call debug_print_list,$(MOCKS_OBJS))
	@echo "All Input Dependency files:"
	@$(call debug_print_list,$(DEP_FILES))
	@echo Stuff to clean:
	@$(call debug_print_list,$(STUFF_TO_CLEAN))
	@echo Includes:
	@$(call debug_print_list,$(INCLUDES))

-include $(OTHER_MAKEFILE_TO_INCLUDE)
