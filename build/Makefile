#**********************************************************************************************
# default environment (if not prepared before)
#**********************************************************************************************
#Set this to @ to keep the makefile quiet
SILENCE 		?= @
BUILD_TARGET		?= target
BUILD_CONFIG		?= rls

BSP_PATH		?= $(HOME)/workspace/bsp
SYSROOT			?= $(BSP_PATH)/sysroots/cortexa8t2hf-neon-netmodule-linux-gnueabi
INST_LIB_DIR		?= $(SYSROOT)/usr/lib
INST_LIB_INC_DIR	?= $(SYSROOT)/usr/include
USE_INITSYS_DUMMY	?= 0
CPUCOUNT		?= $(shell cat /proc/cpuinfo | grep processor | wc -l)
REPO_PATH		?= $(shell git rev-parse --show-toplevel)
MAKEFLAGS 		+= "-j$(CPUCOUNT) -l$(CPUCOUNT)"

CC			?= gcc
CXX			?= g++
AR			?= ar
LD			?= ld
STRIP		?= strip
GDB			?= gdb

#**********************************************************************************************
# project specific configuration
#**********************************************************************************************
APPNAME		 = ssf-mgr
PROJECT_PATH	?= $(shell dirname $(shell pwd))
BUILD_PATH	?= $(PROJECT_PATH)/build
OUT_DIR		?= $(BUILD_PATH)/$(BUILD_CONFIG)/$(BUILD_TARGET)
SOURCE_DIRS	+= $(PROJECT_PATH)/src
INCLUDE_DIRS	+= $(PROJECT_PATH) \
		   $(PROJECT_PATH)/src \
		   $(INST_LIB_INC_DIR) \
		   $(SYSROOT)/usr/include

LIBS		+= nmapp \
		   event \
		   systemd \
		   rt

LIB_DIRS	+= $(INST_LIB_DIR)

SOURCES_C	= $(foreach srcdir, $(SOURCE_DIRS), $(shell find $(srcdir) -name "*.c"))
SOURCES_CXX	= $(foreach srcdir, $(SOURCE_DIRS), $(shell find $(srcdir) -name "*.cpp"))
HEADERS		= $(foreach incdir, $(PROJECT_PATH), $(shell find $(incdir) -name "*.h")) \
		  $(foreach incdir, $(PROJECT_PATH), $(shell find $(incdir) -name "*.hpp"))

OBJ_TARGET	 = $(addprefix $(OUT_DIR), $(subst $(SOURCE_DIRS),,$(SOURCES_C:.c=.o)))
OBJ_TARGET	+= $(addprefix $(OUT_DIR), $(subst $(SOURCE_DIRS),,$(SOURCES_CXX:.cpp=.o)))

#**********************************************************************************************
# compiler and linker flags
#**********************************************************************************************
CFLAGS += --sysroot=$(SYSROOT)
CFLAGS += -rdynamic -funwind-tables -fPIC -D__PLATFORM_LINUX_POSIX
CFLAGS += -Wall -Werror -Wswitch-default -Wconversion -Wswitch-enum -Wno-unused-result
CXXFLAGS += --sysroot=$(SYSROOT)
CXXFLAGS += -rdynamic -funwind-tables -fPIC -D__PLATFORM_LINUX_POSIX
CXXFLAGS += -Wall -Werror -Wswitch-default -Wconversion -Wswitch-enum -Wno-unused-result

ifeq ($(BUILD_CONFIG), rls)
	CFLAGS += -D__RELEASE -O3
	CXXFLAGS += -D__RELEASE -O3
else ifeq ($(BUILD_CONFIG), dbg)
	CFLAGS += -g -D__DEBUG
	CXXFLAGS += -g -D__DEBUG
else
	CFLAGS += -g -D__TEST
	CXXFLAGS += -g -D__TEST
endif

CFLAGS += $(foreach includedir,$(INCLUDE_DIRS),-I$(includedir))
CXXFLAGS += $(foreach includedir,$(INCLUDE_DIRS),-I$(includedir))

LDFLAGS += -export-dynamic
LDFLAGS += $(foreach librarydir,$(LIB_DIRS),-L$(librarydir))
LDFLAGS += $(foreach library,$(LIBS),-l$(library))

#**********************************************************************************************
# make application
#**********************************************************************************************
all: $(APPNAME)


$(OUT_DIR):
	@echo create output directory $(OUT_DIR)...
	@mkdir -p $(OUT_DIR)

$(APPNAME): | $(OUT_DIR) $(OBJ_TARGET)
	@echo Creating $(APPNAME)...
	$(SILENCE)$(CXX) $(OBJ_TARGET) $(LDFLAGS) -o $(OUT_DIR)/$@
	@echo ---------------------------------------
	@echo  $(APPNAME) successfully built
	@echo ---------------------------------------
	@printf "  %-15s: %s\n" "Build config" "$(BUILD_TARGET) - $(BUILD_CONFIG)"
	@printf "  %-15s: %s\n" "Build time" "$(shell date +"%b %e %Y %T")"
	@echo ---------------------------------------

#build c files from source folder
$(OUT_DIR)/%.o: ../src/%.c $(HEADERS)
	@echo $(CC) $@ ...
	$(SILENCE)mkdir -p $(shell dirname $@)
	$(SILENCE)$(CC) -c $< $(CFLAGS) -o $@

#build c++ files from src folder
$(OUT_DIR)/%.o: ../src/%.cpp $(HEADERS)
	@echo $(CXX) $@ ...
	$(SILENCE)mkdir -p $(shell dirname $@)
	$(SILENCE)$(CXX) -c $< $(CXXFLAGS) -o $@


#**********************************************************************************************
# analyze the application
#*********************************************************************************************
analyze:
	@echo Analyzing $(APPNAME)...
	@echo TODO: add static code analysis and metric calculations


#**********************************************************************************************
# clean the application
#*********************************************************************************************
clean:
	@echo Cleaning ...
	rm -rf $(OUT_DIR)

.PHONY: all clean analyze
