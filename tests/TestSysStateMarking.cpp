
extern "C"
{
    #include <errno.h>
    #include <stdio.h>
    #include <string.h>
    #include <signal.h>

    #include <nmapp/exception/exception.h>
    #include <nmapp-test/mocks/SysLogger-Mock.h>
    #include <nmapp-test/logging/SysLogger-private.h>
    #include <nmapp-test/mocks/FileOperation-Mock.h>
    #include <nmapp-test/mocks/UnixInitSys-Mock.h>
    #include <nmapp-test/event/evt-loop-private.h>
    #include "../src/system-state/SysStateMarking_private.h"
}

#include <nmapp-test/utils/NmComparatorsAndCopiers.hpp>


#include "CppUTest/TestHarness.h"
#include <CppUTestExt/MockSupport.h>
#include <CppUTestExt/MockSupport_c.h>



// TESTS:
//------------------------------------------------------
TEST_GROUP(SysStateMarkingTest)
{
    void setup()
    {
        mock().installComparator("char*", charPtrCmp);
        mock().installCopier("char*", charPtrCpy);
        mock().installComparator("const char*", constCharPtrCmp);
        mock().installCopier("const char*", constCharPtrCpy);
        mock().installComparator("UnixInitSysDBusSignalItemCallback", dbusSigCbFnPtrCmp);
        loggerMock =  SysLoggerMock_create("SysStateMarkingTest", LOG_LEVEL_DBG, false);
        sysLogger = SysLoggerMock_getLoggerItf(loggerMock);

        fileOpMock = FileOperationMock_create(sysLogger);
        sysStateUp.fileName = "my/sys/state";
        sysStateUp.fileOpItf = FileOperationMock_getItfRef(fileOpMock);
        initSysMock = UnixInitSysMock_create(sysLogger);
        setupEvtLoop();
        dut = NULL;
    }
    void teardown()
    {
        destroyEvtLoop();
        UnixInitSysMock_destroy(initSysMock);
        FileOperationMock_destroy(fileOpMock);
        SysLoggerMock_destroy(loggerMock);
        mock().checkExpectations();
        mock().clear();
        mock().removeAllComparatorsAndCopiers();
    }
public:
    nm_tests::CharPointerComparator charPtrCmp;
    nm_tests::CharPointerCopier charPtrCpy;
    nm_tests::ConstCharPointerComparator constCharPtrCmp;
    nm_tests::ConstCharPointerCopier constCharPtrCpy;
    nm_tests::UnixInitSysDBusSigItemCbComparator dbusSigCbFnPtrCmp;
    SysLoggerMockRef loggerMock;
    ILogRef sysLogger;
    EvtLoopRef evtLoopRef;
    FileOperationMockRef fileOpMock;
    UnixInitSysMockRef initSysMock;

    SysStateMarkingRef dut;

    FileContainer_t sysStateUp;

    const char* bus_svc = "org.freedesktop.systemd1";
    const char* bus_path = "/org/freedesktop/systemd1";
    const char* bus_itf = "org.freedesktop.systemd1.Manager";
    const char* bus_sysUpSignal = "StartupFinished";
    const char* bus_sysUpItem = "up";
    const char* bus_shutdownSignal = "UnitNew";
    const char* bus_poweroffItem = "poweroff.target";
    const char* bus_rebootItem = "reboot.target";

    //----------------------------------------------------------------
    void setupEvtLoop(void)
    {
        mock().expectOneCall("event_base_new").andReturnValue((void*)42);
        mock().expectOneCall("event_base_get_features").andReturnValue((int)0x20);
        evtLoopRef = EvtLoop_create(sysLogger, false);
    }
    void destroyEvtLoop(void)
    {
        mock().expectOneCall("event_base_free");
        EvtLoop_destroy(evtLoopRef);
        evtLoopRef = NULL;
    }

    //----------------------------------------------------------------
    void expectedInitCalls(void)
    {
        mock().expectOneCall("IUnixInitSys_initDBus");
        mock().expectOneCall("IUnixInitSys_getEventFileDescriptor")
                .andReturnValue(42);
        mock().expectOneCall("event_new")
                .andReturnValue((void*)42);
        mock().expectOneCall("event_add")
                    .andReturnValue((int)0);
        mock().expectOneCall("IUnixInitSys_registerDBusSignalCallback")
                  .withParameterOfType("UnixInitSysDBusSignalItemCallback", "cbFn", (const void*)dbusSignalMatchCallbackFn)
                  .withPointerParameter("pCbCtx", (void*)evtLoopRef);
        mock().expectOneCall("IUnixInitSys_registerDBusSignalMatch")
                    .withStringParameter("dbusService", bus_svc)
                    .withStringParameter("dbusObjPath", bus_path)
                    .withStringParameter("dbusItfName", bus_itf)
                    .withStringParameter("dbusSignalName", bus_sysUpSignal);
        mock().expectOneCall("IUnixInitSys_registerDBusSignalMatch")
                    .withStringParameter("dbusService", bus_svc)
                    .withStringParameter("dbusObjPath", bus_path)
                    .withStringParameter("dbusItfName", bus_itf)
                    .withStringParameter("dbusSignalName", bus_shutdownSignal);
        mock().expectOneCall("IUnixInitSys_activateDBusListener");
    }
    //----------------------------------------------------------------
    void expectedDestroyCalls(void)
    {
        mock().expectOneCall("IUnixInitSys_unregisterDBusSignalMatch")
                .ignoreOtherParameters();
        mock().expectOneCall("IUnixInitSys_unregisterDBusSignalMatch")
                .ignoreOtherParameters();
        mock().expectOneCall("event_del").andReturnValue((int)0);
        mock().expectOneCall("event_free");
        mock().expectOneCall("IUnixInitSys_deinitDBus");
    }
    //----------------------------------------------------------------
    void initDut(void)
    {

        expectedInitCalls();
        SysStateMarking_init(dut, evtLoopRef, UnixInitSysMock_getItfRef(initSysMock), sysStateUp);
    }
    //----------------------------------------------------------------
    void deinitDut(void)
    {
        expectedDestroyCalls();
    }
    //----------------------------------------------------------------
    void setupDut(void)
    {
        dut = SysStateMarking_create(sysLogger);
        CHECK(dut != NULL);
        CHECK_EQUAL(sysLogger, dut->logRef);
    }
    //----------------------------------------------------------------
    void destroyDut(void)
    {
        SysStateMarking_destroy(dut);
    }

};


//--------------------------------------------------------------------------------------------
TEST(SysStateMarkingTest, dbusSignalCallbackWhenRebooting)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        char data[] = "reboot";
        const char* pData = &data[0];
        uint32_t    dataSz = (uint32_t)strlen(pData);
        setupDut();
        initDut();

        mock().expectOneCall("IFileOperation_isExist")
                        .withStringParameter("fileName", dut->systemUpFile.fileName)
                        .andReturnValue(true);
        mock().expectOneCall("IFileOperation_open")
                .withStringParameter("fileName", dut->systemUpFile.fileName)
                .withStringParameter("modes", "w");
        mock().expectOneCall("IFileOperation_writeToFile")
                .withParameterOfType("const char*", "data", pData)
                .withUnsignedIntParameter("size", dataSz);
        mock().expectOneCall("IFileOperation_close");
        onSystemReboot((void*)dut);

        deinitDut();
        destroyDut();
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}
//--------------------------------------------------------------------------------------------
TEST(SysStateMarkingTest, dbusSignalCallbackWhenPoweringOff)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        char data[] = "powerdown";
        const char* pData = &data[0];
        uint32_t    dataSz = (uint32_t)strlen(pData);
        setupDut();
        initDut();

        mock().expectOneCall("IFileOperation_isExist")
                        .withStringParameter("fileName", dut->systemUpFile.fileName)
                        .andReturnValue(true);
        mock().expectOneCall("IFileOperation_open")
                .withStringParameter("fileName", dut->systemUpFile.fileName)
                .withStringParameter("modes", "w");
        mock().expectOneCall("IFileOperation_writeToFile")
                .withParameterOfType("const char*", "data", pData)
                .withUnsignedIntParameter("size", dataSz);
        mock().expectOneCall("IFileOperation_close");
        onSystemPoweroff((void*)dut);

        deinitDut();
        destroyDut();
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}
//--------------------------------------------------------------------------------------------
TEST(SysStateMarkingTest, dbusSignalCallbackWhenUp)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        char data[] = "up";
        const char* pData = &data[0];
        uint32_t    dataSz = (uint32_t)strlen(pData);
        setupDut();
        initDut();

        mock().expectOneCall("IUnixInitSys_getOperatingState").andReturnValue((const char*)"running");
        mock().expectOneCall("IFileOperation_isExist")
                        .withStringParameter("fileName", dut->systemUpFile.fileName)
                        .andReturnValue(true);
        mock().expectOneCall("IFileOperation_open")
                .withStringParameter("fileName", dut->systemUpFile.fileName)
                .withStringParameter("modes", "w");
        mock().expectOneCall("IFileOperation_writeToFile")
                .withParameterOfType("const char*", "data", pData)
                .withUnsignedIntParameter("size", dataSz);
        mock().expectOneCall("IFileOperation_close");
        onSystemUp((void*)dut);

        deinitDut();
        destroyDut();
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}


//--------------------------------------------------------------------------------------------
TEST(SysStateMarkingTest, init)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        setupDut();
        initDut();
        MEMCMP_EQUAL(&sysStateUp, &dut->systemUpFile, sizeof(sysStateUp));
        POINTERS_EQUAL(UnixInitSysMock_getItfRef(initSysMock), dut->initSysItf);
        deinitDut();
        destroyDut();
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}
//--------------------------------------------------------------------------------------------
TEST(SysStateMarkingTest, createAndDestroy)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        setupDut();
        destroyDut();
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}




