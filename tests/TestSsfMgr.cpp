
extern "C"
{
    #include <errno.h>
    #include <stdio.h>
    #include <string.h>
    #include <signal.h>

    #include <nmapp/exception/exception.h>
    #include <nmapp-test/mocks/SysLogger-Mock.h>
    #include <nmapp-test/logging/SysLogger-private.h>
    #include <nmapp-test/mocks/FileOperation-Mock.h>
    #include <nmapp-test/mocks/UnixInitSys-Mock.h>
    #include <nmapp-test/event/evt-loop-private.h>
    #include "../src/system-state/SysStateMarking_private.h"
    #include "../src/watchdog/SystemWatchdog_private.h"
    #include "../src/watchdog/BrokerPinger_private.h"
    #include "../src/watchdog/WatchdogMgr_private.h"
    #include "../src/ssf-mgr.h"
}

#include <nmapp-test/utils/NmComparatorsAndCopiers.hpp>


#include "CppUTest/TestHarness.h"
#include <CppUTestExt/MockSupport.h>
#include <CppUTestExt/MockSupport_c.h>



// TESTS:
//------------------------------------------------------
TEST_GROUP(SsfMgrTest)
{
    void setup()
    {
        mock().installComparator("char*", charPtrCmp);
        mock().installCopier("char*", charPtrCpy);
        mock().installComparator("const char*", constCharPtrCmp);
        mock().installCopier("const char*", constCharPtrCpy);
        mock().installComparator("UnixInitSysDBusSignalItemCallback", dbusSigCbFnPtrCmp);
        loggerMock =  SysLoggerMock_create("SSF-Mgr-Test", LOG_LEVEL_DBG, false);
        sysLogger = SysLoggerMock_getLoggerItf(loggerMock);
        fileOpMock = FileOperationMock_create(sysLogger);
        sysStateUp.fileName = "my/sys/state";
        sysStateUp.fileOpItf = FileOperationMock_getItfRef(fileOpMock);
        brokerPingRequest.fileName  = "my/ping/request";
        brokerPingRequest.fileOpItf = FileOperationMock_getItfRef(fileOpMock);
        brokerPingResponse.fileName  = "my/ping/response";
        brokerPingResponse.fileOpItf = FileOperationMock_getItfRef(fileOpMock);
        sysWatchdog.fileName  = "my/sys/watchdog";
        sysWatchdog.fileOpItf = FileOperationMock_getItfRef(fileOpMock);

        initSysMock = UnixInitSysMock_create(sysLogger);
        SysLogger_setupTestFunctions(&getLoggerInstMock, &addLoggerMock);
        dut = NULL;
    }
    void teardown()
    {
        SysLogger_setupTestFunctions(NULL, NULL);
        UnixInitSysMock_destroy(initSysMock);
        FileOperationMock_destroy(fileOpMock);
        SysLoggerMock_destroy(loggerMock);
        mock().checkExpectations();
        mock().clear();
        mock().removeAllComparatorsAndCopiers();
    }
public:
    nm_tests::CharPointerComparator charPtrCmp;
    nm_tests::CharPointerCopier charPtrCpy;
    nm_tests::ConstCharPointerComparator constCharPtrCmp;
    nm_tests::ConstCharPointerCopier constCharPtrCpy;
    nm_tests::UnixInitSysDBusSigItemCbComparator dbusSigCbFnPtrCmp;
    SysLoggerMockRef loggerMock;
    ILogRef sysLogger;
    FileOperationMockRef fileOpMock;
    UnixInitSysMockRef initSysMock;
    FileContainer_t  sysStateUp;
    FileContainer_t  brokerPingRequest;
    FileContainer_t  brokerPingResponse;
    FileContainer_t  sysWatchdog;
    SystemWatchdogRef sysWatchdogMock;
    SsfMgrRef dut;
    const char* bus_svc = "org.freedesktop.systemd1";
    const char* bus_path = "/org/freedesktop/systemd1";
    const char* bus_itf = "org.freedesktop.systemd1.Manager";
    const char* bus_sysUpSignal = "StartupFinished";
    const char* bus_sysUpItem = "up";
    const char* bus_shutdownSignal = "UnitNew";
    const char* bus_poweroffItem = "poweroff.target";
    const char* bus_rebootItem = "reboot.target";
    bool withMarking = true;
    bool withWatchdog = true;
    uint32_t wdTimeoutMs = 8000;
    uint32_t wdChkIntervalMs = 4000;

    //----------------------------------------------------------------
    void expectedPostInitCalls(void)
    {
        if(withMarking)
        {
            mock().expectOneCall("IUnixInitSys_initDBus");
            mock().expectOneCall("IUnixInitSys_getEventFileDescriptor")
                    .andReturnValue(42);
            mock().expectOneCall("event_new")
                    .andReturnValue((void*)42);
            mock().expectOneCall("event_add")
                        .andReturnValue((int)0);
            mock().expectOneCall("IUnixInitSys_registerDBusSignalCallback")
                      .withParameterOfType("UnixInitSysDBusSignalItemCallback", "cbFn", (const void*)dbusSignalMatchCallbackFn)
                      .withPointerParameter("pCbCtx", (void*)dut->evtLoopRef);
            mock().expectOneCall("IUnixInitSys_registerDBusSignalMatch")
                        .withStringParameter("dbusService", bus_svc)
                        .withStringParameter("dbusObjPath", bus_path)
                        .withStringParameter("dbusItfName", bus_itf)
                        .withStringParameter("dbusSignalName", bus_sysUpSignal);
            mock().expectOneCall("IUnixInitSys_registerDBusSignalMatch")
                        .withStringParameter("dbusService", bus_svc)
                        .withStringParameter("dbusObjPath", bus_path)
                        .withStringParameter("dbusItfName", bus_itf)
                        .withStringParameter("dbusSignalName", bus_shutdownSignal);
            mock().expectOneCall("IUnixInitSys_activateDBusListener");
        }
        if(withWatchdog)
        {
            mock().expectOneCall("event_add").andReturnValue((int)0);
        }
    }
    //----------------------------------------------------------------
    void expectedUnregisteringDBusCalls(void)
    {
        // mark system state:
        if(withMarking)
        {
            mock().expectOneCall("IUnixInitSys_unregisterDBusSignalMatch")
                    .ignoreOtherParameters();
            mock().expectOneCall("IUnixInitSys_unregisterDBusSignalMatch")
                    .ignoreOtherParameters();
        }
    }
    //----------------------------------------------------------------
    void watchdogMgrInitCalls(void)
    {
        if(withWatchdog)
        {
            // broker pinger:
            mock().expectOneCall("event_new").andReturnValue((void*)10);

            // timers:
            mock().expectOneCall("event_new").andReturnValue((void*)11);
        }
    }
    void watchdogMgrDestroyCalls(void)
    {
        if(withWatchdog)
        {
            // timers:
            if(EvtLoop_isTimerRunning(dut->watchdogMgrRef->checkTimerRef))
            {
                mock().expectOneCall("event_del");
            }
            mock().expectOneCall("event_free");

            // broker pinger:
            if(EvtLoop_isTimerRunning(dut->watchdogMgrRef->brokerCheck.pingerRef->pingResonseTimerRef))
            {
                mock().expectOneCall("event_del");
            }
            mock().expectOneCall("event_free");
        }
    }
    //----------------------------------------------------------------
    void sysStateDestroyCalls(void)
    {
        if(withMarking)
        {
            mock().expectOneCall("event_del").andReturnValue((int)0);
            mock().expectOneCall("event_free");
            mock().expectOneCall("IUnixInitSys_deinitDBus");
        }

    }
    //----------------------------------------------------------------
    void expectedInitCalls(void)
    {
        static const char* evtLoopLogName  = LOG_NAME_EVTLOOP;
        mock().expectOneCall("SysLogger_getLoggerInstance")
                .withStringParameter("name", evtLoopLogName)
                .withBoolParameter("addIfMissing", true)
                .andReturnValue((void*)sysLogger);

        // event loop creation
        mock().expectOneCall("event_base_new")
                .andReturnValue((void*)77);
        mock().expectOneCall("event_base_get_features")
                .andReturnValue((int)0x20);

        // signal registering:
        mock().expectNCalls(9, "event_new")
                .andReturnValue((void*)9);
        mock().expectNCalls(9, "event_add")
                    .andReturnValue((int)0);

        // sync pipe:
        mock().expectOneCall("epoll_create1")
                    .andReturnValue((int)0);
        mock().expectOneCall("event_new")
                .andReturnValue((void*)6);
        mock().expectOneCall("event_add")
                    .andReturnValue((int)0);
        mock().expectOneCall("epoll_ctl")
                .andReturnValue((int)0);

        watchdogMgrInitCalls();
    }
    //----------------------------------------------------------------
    void expectedDestroyCalls(void)
    {
        watchdogMgrDestroyCalls();
        sysStateDestroyCalls();

        // sync pipe:
        mock().expectOneCall("epoll_ctl")
                .andReturnValue((int)0);
        mock().expectOneCall("event_del")
                .andReturnValue((int)0);
        mock().expectOneCall("event_free");


        // signal registering:
        mock().expectNCalls(9, "event_del")
                .andReturnValue((int)0);
        mock().expectNCalls(9, "event_free");

        // event loop:
        mock().expectOneCall("event_base_free");
    }
    //----------------------------------------------------------------
    void setupSystemWatchdogMock(void)
    {
        if(withWatchdog)
        {
            sysWatchdogMock = SystemWatchdog_create(sysLogger, sysWatchdog, wdTimeoutMs, true);
            mock().expectOneCall("IFileOperation_isExist")
                    .withStringParameter("fileName", sysWatchdog.fileName)
                    .andReturnValue(true);
            mock().expectOneCall("IFileOperation_isFileOpen")
                    .andReturnValue(false);
            mock().expectOneCall("IFileOperation_open")
                    .withStringParameter("fileName", sysWatchdog.fileName)
                    .withStringParameter("modes", "r+");
            mock().expectOneCall("IFileOperation_getFiledescriptor")
                    .andReturnValue((int)666);
            SysWdMockWdTimeoutS = 1;
            SysWdMockWdStatus = 1;
        }
    }
    void destroySystemWatchdogMockCalls(void)
    {
        if(withWatchdog)
        {
            mock().expectOneCall("IFileOperation_isFileOpen")
                    .andReturnValue(true);
            mock().expectOneCall("IFileOperation_writeToFile")
                    .withParameterOfType("const char*", "data", "V")
                    .withUnsignedIntParameter("size", 1);
            mock().expectOneCall("IFileOperation_close");
        }
    }
    //----------------------------------------------------------------
    void setupDut(void)
    {
        dut = SsfMgr_create("SsfMgrTest", sysLogger, sysLogger, sysLogger, sysLogger, withMarking, withWatchdog,
                            wdTimeoutMs, wdChkIntervalMs);
        CHECK(dut != NULL);
        CHECK_EQUAL(withMarking, dut->isMarkingEnabled);
        CHECK_EQUAL(withWatchdog, dut->isWatchdogEnabled);
        setupSystemWatchdogMock();
        expectedInitCalls();
        SsfMgr_init(dut, false, NULL, UnixInitSysMock_getItfRef(initSysMock), sysStateUp,
                    sysWatchdog, brokerPingRequest, brokerPingResponse);
        expectedPostInitCalls();
        SsfMgr_postDaemonizeInit(dut);
    }
    //----------------------------------------------------------------
    void destroyDut(void)
    {
        expectedUnregisteringDBusCalls();
        expectedDestroyCalls();
        destroySystemWatchdogMockCalls();
        SsfMgr_destroy(dut);
    }

};

//--------------------------------------------------------------------------------------------
TEST(SsfMgrTest, createInitAndDestroyWithoutWatchdog)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        withWatchdog = false;
        setupDut();
        CHECK(dut->sysState != NULL);
        MEMCMP_EQUAL(&sysStateUp, &dut->systemUpFile, sizeof(sysStateUp));
        POINTERS_EQUAL(NULL, dut->watchdogMgrRef);
        destroyDut();
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}
//--------------------------------------------------------------------------------------------
TEST(SsfMgrTest, createInitAndDestroyWithoutMarking)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        withMarking = false;
        setupDut();
        POINTERS_EQUAL(NULL, dut->sysState);
        CHECK(dut->watchdogMgrRef != NULL);
        MEMCMP_EQUAL(&sysWatchdog, &dut->systemWatchdogFile, sizeof(sysWatchdog));
        destroyDut();
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}
//--------------------------------------------------------------------------------------------
TEST(SsfMgrTest, createInitAndDestroyWithMarkingAndWatchdog)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        setupDut();
        CHECK(dut->sysState != NULL);
        MEMCMP_EQUAL(&sysStateUp, &dut->systemUpFile, sizeof(sysStateUp));
        CHECK(dut->watchdogMgrRef != NULL);
        MEMCMP_EQUAL(&sysWatchdog, &dut->systemWatchdogFile, sizeof(sysWatchdog));
        destroyDut();
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}
//--------------------------------------------------------------------------------------------
TEST(SsfMgrTest, createInitAndDestroyWithoutMarkingAndWithoutWatchdog)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        withMarking = false;
        withWatchdog = false;
        setupDut();
        POINTERS_EQUAL(NULL, dut->sysState);
        POINTERS_EQUAL(NULL, dut->watchdogMgrRef);
        destroyDut();
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}


