
extern "C"
{
    #include <nmapp/exception/exception.h>
    #include <nmapp-test/mocks/SysLogger-Mock.h>
    #include <nmapp-test/logging/SysLogger-private.h>
    #include <nmapp-test/mocks/FileOperation-Mock.h>
    #include "../src/FileSystem.h"
    #include "../src/watchdog/SystemWatchdog_private.h"
}

#include <nmapp-test/utils/NmComparatorsAndCopiers.hpp>


#include "CppUTest/TestHarness.h"
#include <CppUTestExt/MockSupport.h>
#include <CppUTestExt/MockSupport_c.h>



// TESTS:
//------------------------------------------------------
TEST_GROUP(SystemWatchdogTest)
{
    void setup()
    {
        mock().installComparator("const char*", constCharPtrCmp);
        loggerMock =  SysLoggerMock_create("SystemWatchdogTest", LOG_LEVEL_DBG, false);
        sysLogger = SysLoggerMock_getLoggerItf(loggerMock);

        fileOpMock = FileOperationMock_create(sysLogger);
        sysWatchdog.fileName  = "my/sys/watchdog";
        sysWatchdog.fileOpItf = FileOperationMock_getItfRef(fileOpMock);

        setupSysWd();
    }
    void teardown()
    {
        destroySysWd();

        FileOperationMock_destroy(fileOpMock);
        SysLoggerMock_destroy(loggerMock);
        mock().checkExpectations();
        mock().clear();
        mock().removeAllComparatorsAndCopiers();
    }
public:
    nm_tests::ConstCharPointerComparator constCharPtrCmp;
    SysLoggerMockRef loggerMock;
    ILogRef sysLogger;
    FileOperationMockRef fileOpMock;
    FileContainer_t  sysWatchdog;
    SystemWatchdogRef sysWd;
    uint32_t sysWdTimeoutMs = 8000;


    //----------------------------------------------------------------
    void setupSysWd(void)
    {
        sysWd = SystemWatchdog_create(sysLogger, sysWatchdog, sysWdTimeoutMs, true);
        assert(sysWd != NULL);
        LONGS_EQUAL(-1, sysWd->fdWd);
        POINTERS_EQUAL(sysLogger, sysWd->logRef);
        MEMCMP_EQUAL(&sysWatchdog, &sysWd->fileContainer,sizeof(sysWatchdog));

        mock().expectOneCall("IFileOperation_isExist")
                .withStringParameter("fileName", sysWatchdog.fileName)
                .andReturnValue(true);
        mock().expectOneCall("IFileOperation_isFileOpen")
                .andReturnValue(false);
        mock().expectOneCall("IFileOperation_open")
                .withStringParameter("fileName", sysWatchdog.fileName)
                .withStringParameter("modes", "r+");
        mock().expectOneCall("IFileOperation_getFiledescriptor")
                .andReturnValue((int)666);
        SysWdMockWdTimeoutS = 1;
        SysWdMockWdStatus = 1;
        SystemWatchdog_init(sysWd);
    }
    void destroySysWd(void)
    {

        mock().expectOneCall("IFileOperation_isFileOpen")
                .andReturnValue(true);
        mock().expectOneCall("IFileOperation_writeToFile")
                .withParameterOfType("const char*", "data", "V")
                .withUnsignedIntParameter("size", 1);
        mock().expectOneCall("IFileOperation_close");
        SystemWatchdog_destroy(sysWd);
        LONGS_EQUAL(-1, SysWatchdog.fdWd);
    }
};


//--------------------------------------------------------------------------------------------
TEST(SystemWatchdogTest, feedWatchdog)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        // NOTE: the system watchdog is created in test setup and destroyed in test teardown

        mock().expectOneCall("IFileOperation_writeToFile")
                .withParameterOfType("const char*", "data", "w")
                .withUnsignedIntParameter("size", 1);
        SystemWatchdog_feed(sysWd);
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}

//--------------------------------------------------------------------------------------------
TEST(SystemWatchdogTest, createDestroy)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        // NOTE: the system watchdog is created in test setup and destroyed in test teardown
        LONGS_EQUAL(SysWdMockWdTimeoutS, sysWd->wdTimeoutS);
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}

