
extern "C"
{
    #include <nmapp/exception/exception.h>
    #include <nmapp-test/mocks/SysLogger-Mock.h>
    #include <nmapp-test/logging/SysLogger-private.h>
    #include <nmapp-test/mocks/FileOperation-Mock.h>
    #include <nmapp-test/event/evt-loop-private.h>
    #include "../src/EvtLoopSyncer.h"
    #include "../src/FileSystem.h"
    #include "../src/watchdog/BrokerPinger_private.h"
    #include "../src/watchdog/SystemWatchdog_private.h"
    #include "../src/watchdog/WatchdogMgr_private.h"
}

#include <nmapp-test/utils/NmComparatorsAndCopiers.hpp>


#include "CppUTest/TestHarness.h"
#include <CppUTestExt/MockSupport.h>
#include <CppUTestExt/MockSupport_c.h>



// TESTS:
//------------------------------------------------------
TEST_GROUP(WatchdogMgrTest)
{
    void setup()
    {
        mock().installComparator("const char*", constCharPtrCmp);
        mock().installCopier("char*", charPtrCpy);
        loggerMock =  SysLoggerMock_create("WatchdogMgrTest", LOG_LEVEL_DBG, false);
        sysLogger = SysLoggerMock_getLoggerItf(loggerMock);

        fileOpMock = FileOperationMock_create(sysLogger);
        brokerPingRequest.fileName  = "my/ping/request";
        brokerPingRequest.fileOpItf = FileOperationMock_getItfRef(fileOpMock);
        brokerPingResponse.fileName  = "my/ping/response";
        brokerPingResponse.fileOpItf = FileOperationMock_getItfRef(fileOpMock);
        sysWatchdog.fileName  = "my/sys/watchdog";
        sysWatchdog.fileOpItf = FileOperationMock_getItfRef(fileOpMock);

        setupEvtLoop();

        setupWatchdogMgr();
    }
    void teardown()
    {
        destroyWatchdogMgr();

        destroyEvtLoop();
        FileOperationMock_destroy(fileOpMock);
        SysLoggerMock_destroy(loggerMock);
        mock().checkExpectations();
        mock().clear();
        mock().removeAllComparatorsAndCopiers();
    }
public:
    nm_tests::ConstCharPointerComparator constCharPtrCmp;
    nm_tests::CharPointerCopier charPtrCpy;
    SysLoggerMockRef loggerMock;
    ILogRef sysLogger;
    FileOperationMockRef fileOpMock;
    EvtLoopRef evtLoopRef;
    EvtLoopSyncerRef evtLoopSyncerRef;
    FileContainer_t brokerPingRequest;
    FileContainer_t brokerPingResponse;
    FileContainer_t sysWatchdog;
    SystemWatchdogRef sysWatchdogMock;
    WatchdogMgrRef watchdogMgrRef;
    uint32_t wdTimeoutMs = 8000;
    uint32_t wdChkIntervalMs = 4000;

    //----------------------------------------------------------------
    void setupEvtLoop(void)
    {
        mock().expectOneCall("event_base_new").andReturnValue((void*)42);
        mock().expectOneCall("event_base_get_features").andReturnValue((int)0x20);
        evtLoopRef = EvtLoop_create(sysLogger, false);
        evtLoopSyncerRef = EvtLoopSyncer_create(sysLogger, evtLoopRef, NULL, NULL);
    }
    void destroyEvtLoop(void)
    {
        EvtLoopSyncer_destroy(evtLoopSyncerRef);
        evtLoopSyncerRef = NULL;
        mock().expectOneCall("event_base_free");
        EvtLoop_destroy(evtLoopRef);
        evtLoopRef = NULL;
    }
    //----------------------------------------------------------------
    void setupSystemWatchdogMock(void)
    {
        sysWatchdogMock = SystemWatchdog_create(sysLogger, sysWatchdog, wdTimeoutMs, true);
        mock().expectOneCall("IFileOperation_isExist")
                .withStringParameter("fileName", sysWatchdog.fileName)
                .andReturnValue(true);
        mock().expectOneCall("IFileOperation_isFileOpen")
                .andReturnValue(false);
        mock().expectOneCall("IFileOperation_open")
                .withStringParameter("fileName", sysWatchdog.fileName)
                .withStringParameter("modes", "r+");
        mock().expectOneCall("IFileOperation_getFiledescriptor")
                .andReturnValue((int)666);
        SysWdMockWdTimeoutS = 1;
        SysWdMockWdStatus = 1;
    }
    void destroySystemWatchdogMockCalls(void)
    {
        mock().expectOneCall("IFileOperation_isFileOpen")
                .andReturnValue(true);
        mock().expectOneCall("IFileOperation_writeToFile")
                .withParameterOfType("const char*", "data", "V")
                .withUnsignedIntParameter("size", 1);
        mock().expectOneCall("IFileOperation_close");
    }
    //----------------------------------------------------------------
    void setupWatchdogMgr(void)
    {
        setupSystemWatchdogMock();

        // broker pinger:
        mock().expectOneCall("event_new").andReturnValue((void*)101);

        // timers:
        mock().expectOneCall("event_new").andReturnValue((void*)10);

        watchdogMgrRef = WatchdogMgr_create(sysLogger, sysLogger, evtLoopRef, evtLoopSyncerRef,
                                            brokerPingRequest, brokerPingResponse, sysWatchdog,
                                            wdTimeoutMs, wdChkIntervalMs);
        assert(watchdogMgrRef != NULL);
    }
    void destroyWatchdogMgr(void)
    {
        // timers:
        if(EvtLoop_isTimerRunning(watchdogMgrRef->checkTimerRef))
        {
            mock().expectOneCall("event_del");
        }
        mock().expectOneCall("event_free");

        // broker pinger:
        if(EvtLoop_isTimerRunning(watchdogMgrRef->brokerCheck.pingerRef->pingResonseTimerRef))
        {
            mock().expectOneCall("event_del");
        }
        mock().expectOneCall("event_free");

        // system watchdog:
        destroySystemWatchdogMockCalls();

        WatchdogMgr_destroy(watchdogMgrRef);
    }

};


//--------------------------------------------------------------------------------------------
TEST(WatchdogMgrTest, checkTimerElapsed)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        // NOTE: the watchdog manager is created in test setup and destroyed in test teardown

        // stop check timer
        mock().expectOneCall("event_del");

        // ping broker
        mock().expectOneCall("IFileOperation_open")
                .withStringParameter("fileName", watchdogMgrRef->brokerCheck.pingerRef->pingRequest.fileName)
                .withStringParameter("modes", "w");
        mock().expectOneCall("IFileOperation_writeToFile").ignoreOtherParameters();
        mock().expectOneCall("IFileOperation_close");
        mock().expectOneCall("event_add").andReturnValue((int)0);

        // restart check timer as it is periodic
        mock().expectOneCall("event_add").andReturnValue((int)0);

        checkTimerElapsed((void*)watchdogMgrRef);
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}


//--------------------------------------------------------------------------------------------
TEST(WatchdogMgrTest, startSupervision)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        // NOTE: the watchdog manager is created in test setup and destroyed in test teardown

        mock().expectOneCall("event_add")
                .andReturnValue((int)0);

        WatchdogMgr_start(watchdogMgrRef);
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}
//--------------------------------------------------------------------------------------------
TEST(WatchdogMgrTest, analyzeInvalidPingStatus)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        // NOTE: the watchdog manager is created in test setup and destroyed in test teardown

        // event data:
        Event_t tstEvt;
        tstEvt.dataSize = (uint32_t)strlen(PING_STATUS_NOK);
        snprintf(&tstEvt.data[0], EVTLOOP_SYNCER_EVENT_DATA_SIZE, "%s", PING_STATUS_NOK);
        // stop check timer:
        mock().expectOneCall("event_del");

        WatchdogMgr_analyzePingStatus(&tstEvt.data[0], tstEvt.dataSize, watchdogMgrRef);
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}
//--------------------------------------------------------------------------------------------
TEST(WatchdogMgrTest, analyzeValidPingStatus)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        // NOTE: the watchdog manager is created in test setup and destroyed in test teardown

        // event data:
        Event_t tstEvt;
        tstEvt.dataSize = (uint32_t)strlen(PING_STATUS_OK);
        snprintf(&tstEvt.data[0], EVTLOOP_SYNCER_EVENT_DATA_SIZE, "%s", PING_STATUS_OK);
        // feeding watchdog:
        mock().expectOneCall("IFileOperation_writeToFile")
                .withParameterOfType("const char*", "data", "w")
                .withUnsignedIntParameter("size", 1);

        WatchdogMgr_analyzePingStatus(&tstEvt.data[0], tstEvt.dataSize, watchdogMgrRef);
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}



//--------------------------------------------------------------------------------------------
TEST(WatchdogMgrTest, createDestroy)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        // NOTE: the watchdog manager is created in test setup and destroyed in test teardown
        POINTERS_EQUAL(evtLoopSyncerRef, watchdogMgrRef->syncerRef);
        MEMCMP_EQUAL(&brokerPingRequest, &watchdogMgrRef->brokerCheck.pingRequest, sizeof(FileContainer_t));
        MEMCMP_EQUAL(&brokerPingResponse, &watchdogMgrRef->brokerCheck.pingResponse, sizeof(FileContainer_t));
        MEMCMP_EQUAL(&sysWatchdog, &watchdogMgrRef->sysWatchdog->fileContainer, sizeof(FileContainer_t));
        CHECK(watchdogMgrRef->brokerCheck.pingerRef != NULL);
        CHECK(watchdogMgrRef->sysWatchdog != NULL);
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}

