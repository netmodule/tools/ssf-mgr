
#include "CppUTest/CommandLineTestRunner.h"

#include <unistd.h>

int main(int ac, char** av)
{
    return CommandLineTestRunner::RunAllTests(ac, av);
}

