
extern "C"
{
    #include <nmapp/exception/exception.h>
    #include <nmapp-test/mocks/SysLogger-Mock.h>
    #include <nmapp-test/logging/SysLogger-private.h>
    #include <nmapp-test/mocks/FileOperation-Mock.h>
    #include "../src/EvtLoopSyncer.h"
    #include "../src/FileSystem.h"
    #include "../src/watchdog/BrokerPinger_private.h"
}

#include <nmapp-test/utils/NmComparatorsAndCopiers.hpp>


#include "CppUTest/TestHarness.h"
#include <CppUTestExt/MockSupport.h>
#include <CppUTestExt/MockSupport_c.h>



// TESTS:
//------------------------------------------------------
TEST_GROUP(BrokerPingerTest)
{
    void setup()
    {
        mock().installComparator("const char*", constCharPtrCmp);
        mock().installCopier("char*", charPtrCpy);
        loggerMock =  SysLoggerMock_create("BrokerPingerTest", LOG_LEVEL_DBG, false);
        sysLogger = SysLoggerMock_getLoggerItf(loggerMock);

        fileOpMock = FileOperationMock_create(sysLogger);
        brokerPingRequest.fileName  = "my/ping/request";
        brokerPingRequest.fileOpItf = FileOperationMock_getItfRef(fileOpMock);
        brokerPingResponse.fileName  = "my/ping/response";
        brokerPingResponse.fileOpItf = FileOperationMock_getItfRef(fileOpMock);
        setupEvtLoop();

        setupBrokerPinger();
    }
    void teardown()
    {
        destroyBrokerPinger();

        destroyEvtLoop();
        FileOperationMock_destroy(fileOpMock);
        SysLoggerMock_destroy(loggerMock);
        mock().checkExpectations();
        mock().clear();
        mock().removeAllComparatorsAndCopiers();
    }
public:
    nm_tests::ConstCharPointerComparator constCharPtrCmp;
    nm_tests::CharPointerCopier charPtrCpy;
    SysLoggerMockRef loggerMock;
    ILogRef sysLogger;
    FileOperationMockRef fileOpMock;
    EvtLoopRef evtLoopRef;
    EvtLoopSyncerRef evtLoopSyncerRef;

    BrokerPingerRef  brokerPingerRef;
    FileContainer_t  brokerPingRequest;
    FileContainer_t  brokerPingResponse;

    //----------------------------------------------------------------
    void setupEvtLoop(void)
    {
        mock().expectOneCall("event_base_new").andReturnValue((void*)42);
        mock().expectOneCall("event_base_get_features").andReturnValue((int)0x20);
        evtLoopRef = EvtLoop_create(sysLogger, false);

        evtLoopSyncerRef = EvtLoopSyncer_create(sysLogger, evtLoopRef, NULL, NULL);
    }
    void destroyEvtLoop(void)
    {
        EvtLoopSyncer_destroy(evtLoopSyncerRef);
        evtLoopSyncerRef = NULL;
        mock().expectOneCall("event_base_free");
        EvtLoop_destroy(evtLoopRef);
        evtLoopRef = NULL;
    }
    //----------------------------------------------------------------
    void setupBrokerPinger(void)
    {
        mock().expectOneCall("event_new").andReturnValue((void*)101);
        brokerPingerRef = BrokerPinger_create(evtLoopRef, evtLoopSyncerRef, sysLogger, brokerPingRequest, brokerPingResponse);
    }
    void destroyBrokerPinger(void)
    {
        if(EvtLoop_isTimerRunning(brokerPingerRef->pingResonseTimerRef))
        {
            mock().expectOneCall("event_del");
        }
        mock().expectOneCall("event_free");
        BrokerPinger_destroy(brokerPingerRef);
    }

};


//--------------------------------------------------------------------------------------------
TEST(BrokerPingerTest, gotValidResponseFromBroker)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        // NOTE: the broker is created in test setup and destroyed in test teardown

        enum { TEST_BUF_LEN = 32 };
        char buffer[32] = "123456";
        brokerPingerRef->pingValue = 123456;

        EvtLoopSyncer_enableMockedWrite(evtLoopSyncerRef, true);

        mock().expectOneCall("event_del");
        mock().expectOneCall("IFileOperation_open")
                .withStringParameter("fileName", brokerPingerRef->pingResponse.fileName)
                .withStringParameter("modes", "r");
        mock().expectOneCall("IFileOperation_readFromFile")
                .withOutputParameterOfTypeReturning("char*", "buffer", buffer)
                .withUnsignedIntParameter("size", (unsigned int)(TEST_BUF_LEN-1))
                .withUnsignedIntParameter("offset", (unsigned int)0);
        mock().expectOneCall("IFileOperation_close");

        pingResponseDelayTimerElapsed(brokerPingerRef);

        EvtLoopSyncer_enableMockedWrite(evtLoopSyncerRef, false);
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}
//--------------------------------------------------------------------------------------------
TEST(BrokerPingerTest, sendPingToBroker)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        // NOTE: the broker is created in test setup and destroyed in test teardown

        mock().expectOneCall("IFileOperation_open")
                .withStringParameter("fileName", brokerPingerRef->pingRequest.fileName)
                .withStringParameter("modes", "w");
        mock().expectOneCall("IFileOperation_writeToFile").ignoreOtherParameters();
        mock().expectOneCall("IFileOperation_close");
        mock().expectOneCall("event_add").andReturnValue((int)0);
        BrokerPinger_ping(brokerPingerRef);
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}

//--------------------------------------------------------------------------------------------
TEST(BrokerPingerTest, createDestroyBrokerPinger)
{
    Exception_t e;
    e.id = EX_MAX_EXCEPTIONS;

    Try
    {
        // NOTE: the broker is created in test setup and destroyed in test teardown
        CHECK(brokerPingerRef != NULL);
        POINTERS_EQUAL(evtLoopSyncerRef, brokerPingerRef->resultSyncerRef);
        MEMCMP_EQUAL(&brokerPingRequest, &brokerPingerRef->pingRequest, sizeof(FileContainer_t));
        MEMCMP_EQUAL(&brokerPingResponse, &brokerPingerRef->pingResponse, sizeof(FileContainer_t));
    }
    Catch(e)
    {
        printf("exception %d - %s\n", e.id, e.msg);
        fflush(stdout);
        FAIL("Caught an exception");
    }
}




