/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * FileSystem MODULE
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : FileSystem.h
 * CREATION DATE : Aug 26, 2021
 * TARGET        : <Target: generic,embedded Linux,...>
 *
 *********************************************************************************************/
#ifndef SRC_FILESYSTEM_H_
#define SRC_FILESYSTEM_H_

/** @ingroup SsfManager
 * @defgroup FileSystem File System
 * Provides date types related to the file system
 */

#include <nmapp/file/IFileOperation.h>


#ifdef __cplusplus
extern "C" {
#endif

/*********************************************************************************************
 *        GLOBAL DECLARATIONS
 *********************************************************************************************/

/** SSF system state file path **/
#define SSF_STATE_FILEPATH                  "/sys/kernel/broker/system-state-target"

/** SSF broker ping request file path **/
#define SSF_BROKER_PING_REQUEST_FILEPATH    "/sys/kernel/broker/ping-request"

/** SSF broker ping request file path **/
#define SSF_BROKER_PING_RESPONSE_FILEPATH   "/sys/kernel/broker/ping-response"

/** System Watchdog Device **/
#define WATCHDOGDEV                         "/dev/watchdog"



/** File Container handling data residing in a file **/
typedef struct __FileContainer
{
    const char*       fileName;
    IFileOperationRef fileOpItf;
} FileContainer_t;


/*********************************************************************************************
 *        GLOBAL FUNCTIONS
 *********************************************************************************************/


#ifdef __cplusplus
}
#endif

#endif /* SRC_FILESYSTEM_H_ */
