/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * System State Framework Manager Main
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : main.c
 * CREATION DATE : Aug 23, 2021
 * TARGET        : embedded Linux
 *
 *********************************************************************************************/

#include <errno.h>
#include <stdlib.h>                    // for calloc, free, exit and its status
#include <getopt.h>                    // for options
#include <libgen.h>                    // for basename

#include <nmapp/exception/exception.h>
#include <nmapp/sys/assertion.h>
#include <nmapp/logging/SysLogger.h>
#include <nmapp/conversion/conversion.h>
#include <nmapp/file/FileOperation.h>
#include <nmapp/sys/UnixInitSys.h>
#include "ssf-mgr.h"


/*********************************************************************************************
 *        LOCALS
 *********************************************************************************************/

// Constants:
//------------------------
#define        DEFAULT_APPNAME                 "ssf-mgr"

// System State Marking:
const char*    SYS_STATE_TARGET_FILEPATH     = SSF_STATE_FILEPATH;

// Watchdog Manager:
const char*    WD_BROKER_PING_REQ_FILEPATH   = SSF_BROKER_PING_REQUEST_FILEPATH;
const char*    WD_BROKER_PING_RESP_FILEPATH  = SSF_BROKER_PING_RESPONSE_FILEPATH;
const char*    WD_SYSTEM_WATCHDOG_FILEPATH   = WATCHDOGDEV;


static const struct option APP_LONGOPTS[] =
{
    {"help",             no_argument,        NULL, 'h'},
    {"daemonize",        no_argument,        NULL, 'd'},
    {"pidfile",          required_argument,  NULL, 'p'},
    {"mark-sys-state",   no_argument,        NULL, 'm'},
    {"with-watchdog",    no_argument,        NULL, 'w'},
    {"wd-timeout",       required_argument,  NULL, 't'},
    {"wd-feed-interval", required_argument,  NULL, 'i'},
    SYSLOGGER_LONGOPTS,
    {NULL,        0,                  NULL,  0 }
};
static const char* APP_SHORTOPTS = "hdp:mwt:i:";

// Containers:
//------------------------
typedef struct FileManipulator
{
    FileContainer_t  handle;
    FileOperationRef opRef;
} FileManipulator_t;


// Variables:
//------------------------
static const char*      myAppName                = DEFAULT_APPNAME;
static bool             hasToDaemonize           = false;
static bool             hasToMarkSysState        = false;
static bool             hasToSupervise           = false;
static const char*      myPidFilePath            = NULL;
static SysLoggerRef     mySysLogger              = NULL;
static SsfMgrRef        mySsfMgrRef              = NULL;
static uint32_t         myWatchdogTimeoutMS      = (uint32_t)DEFAULT_WD_TIMEOUT_MS;
static uint32_t         myWatchdogFeedIntervalMS = (uint32_t)DEFAULT_WD_CHECK_INTERVAL_MS;

// System State Marking:
static UnixInitSysRef     myInitSysRef     = NULL;
static FileManipulator_t  mySysStateWriter = {.handle={NULL,NULL}, .opRef = NULL};

// Watchdog:
static FileManipulator_t  myBrokerPingRequester = {.handle={NULL,NULL}, .opRef = NULL};
static FileManipulator_t  myBrokerPingResponser = {.handle={NULL,NULL}, .opRef = NULL};
static FileManipulator_t  mySystemWatchdog      = {.handle={NULL,NULL}, .opRef = NULL};

//--------------------------------------------------------------------------------------------
// Functions:
//------------------------
//--------------------------------------------------------------------------------------------
static void printAppUsage(void)
{
    printf("Usage: %s [args]\n\n"
           "    -h | --help                            Show this help\n"
           "    -d | --daemonize                       Run as daemon\n"
           "    -p | --pidfile=path                    The PID file, see -d\n"
           "    -m | --mark-sys-state                  Mark the system state for the SSF\n"
           "    -w | --with-watchdog                   Enable watchdog and supervise SSF modules\n"
           "    -t | --wd-timeout=TIMEOUT_MS           Configure watchdog timeout to TIMEOUT_MS\n"
           "                                             default=%ums\n"
           "    -i | --wd-feed-interval=INTERVAL_MS    Set watchdog feed interval to INTERVAL_MS\n"
           "                                             default=%ums\n"
           "\n", myAppName, (unsigned int)myWatchdogTimeoutMS, (unsigned int)myWatchdogFeedIntervalMS
    );
    const char* evtLoopLogName    = LOG_NAME_EVTLOOP;
    const char* initSysLogName    = LOG_NAME_INITSYS;
    const char* sysStateLogName   = LOG_NAME_SYSSTATE;
    const char* wdMgrLogName      = LOG_NAME_WD_MGR;
    const char* brokerPingLogName = LOG_NAME_BROKER_PINGER;
    printf("Used loggers: - %s\n"
           "              - %s\n"
           "              - %s\n"
           "              - %s\n"
           "              - %s\n\n", evtLoopLogName,
                                     initSysLogName, sysStateLogName,
                                     wdMgrLogName, brokerPingLogName);
    SysLogCmdLine_printHelp();
}
//--------------------------------------------------------------------------------------------
void handleParsedArgument(int parsedShortArg, int argc, char **argv, int optIdx)
{
    switch (parsedShortArg)
    {
        case 'h':
            printAppUsage();
            exit(EXIT_SUCCESS);
        case 'd':
            hasToDaemonize = true;
            break;
        case 'p':
            myPidFilePath = (const char*)optarg;
            break;
        case 'm':
            hasToMarkSysState = true;
            break;
        case 'w':
            hasToSupervise = true;
            break;
        case 't':
            myWatchdogTimeoutMS = str2uint(optarg);
            break;
        case 'i':
            myWatchdogFeedIntervalMS = str2uint(optarg);
            break;
        case '?':
        default:
        {
            if(!SysLogCmdLine_isValidCommandLineOption(argv[optind - 1]))
            {
                printAppUsage();
                exit(EXIT_FAILURE);
            }
        }
    } // end switch
}
//--------------------------------------------------------------------------------------------
SysLoggerRef parseAppArgumentsAndGetLoggerConfig(int argc, char **argv)
{
    Exception_t  ex;
    SysLoggerRef loggerRef = NULL;
    int c;
    int optIdx = 0;

    Try
    {
        while(true)
        {
            c = getopt_long(argc, argv, APP_SHORTOPTS, APP_LONGOPTS, &optIdx);
            if (c == -1)
            {
                break;
            }
            handleParsedArgument(c, argc, argv, optIdx);
        }

        loggerRef = SysLogger_create(myAppName, LOG_LEVEL_INFO);
        SysLoggersCmdLine_t loggerCfg = SysLogCmdLine_parse(argc, argv);
        SysLogger_addCommandLineLoggers(&loggerCfg);
    }
    Catch(ex)
    {
        fprintf(stderr, "Failed parsing application arguments: %s", ex.msg);
        exit(EXIT_FAILURE);
    }
    return loggerRef;
}
//--------------------------------------------------------------------------------------------
void buildingApp(void)
{
    ILogRef logRef = SysLogger_getLoggerItf(mySysLogger);
    ILog_msg(logRef, LOG_LEVEL_INFO, "%s starting with options: mark-sys-up=%u, with-watchdog=%u, "
                                     "wd-timeout=%ums, wd-interval=%ums",
                                     myAppName, (unsigned int)hasToMarkSysState, (unsigned int)hasToSupervise,
                                     (unsigned int)myWatchdogTimeoutMS, (unsigned int)myWatchdogFeedIntervalMS);

    ILogRef initSysLogRef      = SysLogger_getLoggerInstance((const char*)LOG_NAME_INITSYS, true);
    ILogRef sysStateLogRef     = SysLogger_getLoggerInstance((const char*)(LOG_NAME_SYSSTATE), true);
    ILogRef wdMgrLogRef        = SysLogger_getLoggerInstance((const char*)LOG_NAME_WD_MGR, true);
    ILogRef brokerPingerLogRef = SysLogger_getLoggerInstance((const char*)LOG_NAME_BROKER_PINGER, true);

    mySsfMgrRef = SsfMgr_create(myAppName, logRef, sysStateLogRef, wdMgrLogRef, brokerPingerLogRef,
                                hasToMarkSysState, hasToSupervise, myWatchdogTimeoutMS, myWatchdogFeedIntervalMS);

    // Start-up Marking part:
    if(hasToMarkSysState)
    {
        mySysStateWriter.opRef            = FileOperation_create(sysStateLogRef);
        mySysStateWriter.handle.fileName  = SYS_STATE_TARGET_FILEPATH;
        mySysStateWriter.handle.fileOpItf = FileOperation_getItfRef(mySysStateWriter.opRef);
        myInitSysRef = UnixInitSys_create(initSysLogRef, myAppName, true);
        ILog_msg(logRef, LOG_LEVEL_DBG, "Created InitSys instance and FileOperation for %s", mySysStateWriter.handle.fileName);
    }

    // Watchdog part:
    if(hasToSupervise)
    {
        mySystemWatchdog.opRef            = FileOperation_create(wdMgrLogRef);
        mySystemWatchdog.handle.fileName  = WD_SYSTEM_WATCHDOG_FILEPATH;
        mySystemWatchdog.handle.fileOpItf = FileOperation_getItfRef(mySystemWatchdog.opRef);

        myBrokerPingRequester.opRef            = FileOperation_create(brokerPingerLogRef);
        myBrokerPingRequester.handle.fileName  = WD_BROKER_PING_REQ_FILEPATH;
        myBrokerPingRequester.handle.fileOpItf = FileOperation_getItfRef(myBrokerPingRequester.opRef);

        myBrokerPingResponser.opRef            = FileOperation_create(brokerPingerLogRef);
        myBrokerPingResponser.handle.fileName  = WD_BROKER_PING_RESP_FILEPATH;
        myBrokerPingResponser.handle.fileOpItf = FileOperation_getItfRef(myBrokerPingResponser.opRef);

        ILog_msg(logRef, LOG_LEVEL_DBG, "Created FileOperations for %s, %s and %s",
                                         mySystemWatchdog.handle.fileName,
                                         myBrokerPingRequester.handle.fileName,
                                         myBrokerPingResponser.handle.fileName);
    }
}
//--------------------------------------------------------------------------------------------
int cleanupApp(void)
{
    Exception_t ex;
    bool isCleaningSuccessful = true;
    Try
    {
        SsfMgr_destroy(mySsfMgrRef);
        if(hasToMarkSysState)
        {
            UnixInitSys_destroy(myInitSysRef);
            FileOperation_destroy(mySysStateWriter.opRef);
        }
        if(hasToSupervise)
        {
            FileOperation_destroy(myBrokerPingResponser.opRef);
            FileOperation_destroy(myBrokerPingRequester.opRef);
            FileOperation_destroy(mySystemWatchdog.opRef);
        }
        SysLogger_destroy(mySysLogger);
    }
    Catch(ex)
    {
        fprintf(stderr, "Failed cleaning application: %s", ex.msg);
        isCleaningSuccessful = false;
    }
    return isCleaningSuccessful ? 0 : -1;
}
//--------------------------------------------------------------------------------------------
void daemonizeIfNeeded(bool hasToDaemonize)
{
    assert(mySsfMgrRef != NULL);
    assert(mySysLogger != NULL);

    if(hasToDaemonize == false)
    {
        // Daemonizing is not requested
        return;
    }

    if(SsfMgr_daemonize(mySsfMgrRef) == false)
    {
        ILogRef logRef = SysLogger_getLoggerItf(mySysLogger);
        ILog_msg(logRef, LOG_LEVEL_DBG, "Parent is exiting, cleaning up...");
        if(cleanupApp() != 0)
        {
            exit(EXIT_FAILURE);
        }
        exit(EXIT_SUCCESS);
    }
}

/*********************************************************************************************
 *        GLOBALS
 *********************************************************************************************/
//--------------------------------------------------------------------------------------------
int main(int argc, char **argv)
{
    Exception_t ex;
    int         appRetVal = 0;
    myAppName   = (const char*)basename(argv[0]);
    mySysLogger = parseAppArgumentsAndGetLoggerConfig(argc, argv);
    assert(mySysLogger != NULL);

    Try
    {
        buildingApp();

        // Initialize the created instances:
        IUnixInitSysRef unixInitSysItfRef = hasToMarkSysState ? UnixInitSys_getItfRef(myInitSysRef) : NULL;
        SsfMgr_init(mySsfMgrRef, hasToDaemonize, myPidFilePath, unixInitSysItfRef, mySysStateWriter.handle,
                    mySystemWatchdog.handle, myBrokerPingRequester.handle, myBrokerPingResponser.handle);

        // Daemonizing and do necessary post inits:
        daemonizeIfNeeded(hasToDaemonize);
        SsfMgr_postDaemonizeInit(mySsfMgrRef);

        // Run:
        SsfMgr_run(mySsfMgrRef);
    }
    Catch(ex)
    {
        ILogRef logRef = SysLogger_getLoggerItf(mySysLogger);
        ILog_msg(logRef, LOG_LEVEL_CRIT, "%s", ex.msg);
        appRetVal = -1;
    }

    appRetVal += cleanupApp();
    mySsfMgrRef = NULL;
    mySysLogger = NULL;
    return appRetVal;
}
