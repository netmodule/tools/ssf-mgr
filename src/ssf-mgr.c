/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * SSF Manager
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : ssf-mgr.c
 * CREATION DATE : Augg 23, 2021
 * TARGET        : embedded Linux
 *
 *********************************************************************************************/

#include <stdlib.h>                    // for calloc, free, exit, ...
#include <signal.h>                    // for signal SIGSEGV
#include <execinfo.h>                  // for backtrace
#include <errno.h>                     // for error numbers
#include <string.h>                    // for strerror

#include <nmapp/exception/exception.h>
#include <nmapp/sys/assertion.h>
#include <nmapp/process/daemonizer.h>
#include "ssf-mgr.h"

/*********************************************************************************************
 *        LOCALS
 *********************************************************************************************/


//--------------------------------------------------------------------------------------------
static void onSegmentationFault(void* calleCtx)
{
    SsfMgrRef me = (SsfMgrRef)calleCtx;
    assert(me != NULL);

    enum { MAX_BACKTRACES = 16 };
    void* bt[MAX_BACKTRACES];
    int   size;
    ILog_msg(me->sysLogRef, LOG_LEVEL_EMCY, "Segmentation fault caught");
    ILog_msg(me->sysLogRef, LOG_LEVEL_EMCY, "----- Backtrace of SIGSEGV -----");
    size = backtrace(bt, MAX_BACKTRACES);
    ILog_msg(me->sysLogRef, LOG_LEVEL_EMCY, "[BT]: Found %d traces", size);
    char **btStrings = backtrace_symbols(bt, size);
    if(NULL != btStrings)
    {
        int i;
        for(i = 0; i < size; i++)
        {
            ILog_msg(me->sysLogRef, LOG_LEVEL_EMCY, "[BT]: %s", btStrings[i]);
        }
        ILog_msg(me->sysLogRef, LOG_LEVEL_EMCY, "------------------------------------------");
        free(btStrings);
        return;
    }
    ILog_msg(me->sysLogRef, LOG_LEVEL_EMCY, "[BT]: cannot malloc space for backtrace_symbols");
}
//--------------------------------------------------------------------------------------------
static void onHardTerminationSignal(SsfMgrRef me, int signalNbr)
{
    assert(me != NULL);
    ILog_msg(me->sysLogRef, LOG_LEVEL_ERR, "Hard termination: Caught signal %d - %s", signalNbr, strsignal(signalNbr));
    exit(EXIT_FAILURE);
}
//--------------------------------------------------------------------------------------------
static void onIllegalInstruction(void* calleCtx)
{
    SsfMgrRef me = (SsfMgrRef)calleCtx;
    assert(me != NULL);
    onHardTerminationSignal(me, SIGILL);
}
//--------------------------------------------------------------------------------------------
static void onAbort(void* calleCtx)
{
    SsfMgrRef me = (SsfMgrRef)calleCtx;
    assert(me != NULL);
    onHardTerminationSignal(me, SIGABRT);
}
//--------------------------------------------------------------------------------------------
static void onArithmeticOperationError(void* calleCtx)
{
    SsfMgrRef me = (SsfMgrRef)calleCtx;
    assert(me != NULL);
    onHardTerminationSignal(me, SIGFPE);
}
//--------------------------------------------------------------------------------------------
static void onBusError(void* calleCtx)
{
    SsfMgrRef me = (SsfMgrRef)calleCtx;
    assert(me != NULL);
    onHardTerminationSignal(me, SIGBUS);
}
//--------------------------------------------------------------------------------------------
static void onBadSystemCall(void* calleCtx)
{
    SsfMgrRef me = (SsfMgrRef)calleCtx;
    assert(me != NULL);
    onHardTerminationSignal(me, SIGSYS);
}
//--------------------------------------------------------------------------------------------
static void onHangup(void* calleCtx)
{
    SsfMgrRef me = (SsfMgrRef)calleCtx;
    assert(me != NULL);
    ILog_msg(me->sysLogRef, LOG_LEVEL_WARN, "Caught signal %d - %s", SIGHUP, strsignal(SIGHUP));
}



//--------------------------------------------------------------------------------------------
static void syncEventDispatcher(const char *pData, uint32_t dataSize, GenericTypeRef pUsrCtx)
{
    SsfMgrRef me = (SsfMgrRef)pUsrCtx;
    ILog_msg(me->sysLogRef, LOG_LEVEL_DBG, "Sync Event dispatcher called");
    assert(pData != NULL);

    Event_t evt;
    memcpy(&evt, pData, sizeof(evt));       // copy pData to event preventing critical multithread issues

    if(evt.id == SSF_PING_RESPONSE_ID)
    {
        WatchdogMgr_analyzePingStatus(&evt.data[0], dataSize, me->watchdogMgrRef);
    }

    // the rest of the events are not handled
}


//--------------------------------------------------------------------------------------------
void registerSignals(SsfMgrRef me)
{
    assert(me != NULL);

    EvtLoop_registerSignalEvent(me->evtLoopRef, SIGSEGV, &onSegmentationFault, (void*)me);
    EvtLoop_registerSignalEvent(me->evtLoopRef, SIGILL,  &onIllegalInstruction, (void*)me);
    EvtLoop_registerSignalEvent(me->evtLoopRef, SIGABRT, &onAbort, (void*)me);
    EvtLoop_registerSignalEvent(me->evtLoopRef, SIGFPE,  &onArithmeticOperationError, (void*)me);
    EvtLoop_registerSignalEvent(me->evtLoopRef, SIGBUS,  &onBusError, (void*)me);
    EvtLoop_registerSignalEvent(me->evtLoopRef, SIGSYS,  &onBadSystemCall, (void*)me);
    EvtLoop_registerSignalEvent(me->evtLoopRef, SIGHUP,  &onHangup, (void*)me);
    ILog_msg(me->sysLogRef, LOG_LEVEL_DBG, "Basic signals registered");
}


//--------------------------------------------------------------------------------------------
void createAndSetupEventLoop(SsfMgrRef me)
{
    assert(me != NULL);
    me->evtLoopRef = EvtLoop_create(me->evtLoopLogRef, true);
    registerSignals(me);
    me->evtLoopSyncerRef = EvtLoopSyncer_create(me->evtLoopLogRef, me->evtLoopRef, &syncEventDispatcher, me);
    ILog_msg(me->sysLogRef, LOG_LEVEL_DBG, "Event loop created and basically setup");
    return;
}



/*********************************************************************************************
 *        GLOBALS
 *********************************************************************************************/
//--------------------------------------------------------------------------------------------
SsfMgrRef SsfMgr_create(const char* appName, ILogRef logRef,
                        ILogRef sysStateLogRef, ILogRef watchdogMgrLogRef, ILogRef brokerPingerLogRef,
                        bool isMarkingUp, bool isSupervising, uint32_t wdTimeoutMS, uint32_t wdChkIntervalMS)
{
    SsfMgrRef me = (SsfMgrRef)calloc(1, sizeof(SsfMgrPrivate_t));
    if(me == NULL)
    {
        Throw(EX_CREATE, "Could not create SSF manager instance");
    }
    me->appName = appName;
    me->sysLogRef = logRef;
    me->sysStateLogRef = sysStateLogRef;
    me->wdMgrLogRef    = watchdogMgrLogRef;
    me->pingLogRef     = brokerPingerLogRef;
    me->isMarkingEnabled  = isMarkingUp;
    me->isWatchdogEnabled = isSupervising;
    me->watchdogTimeoutMS       = wdTimeoutMS;
    me->watchdogCheckIntervalMS = wdChkIntervalMS;


    ILog_msg(me->sysLogRef, LOG_LEVEL_DBG, "Instance for %s created", me->appName);
    return me;
}
//--------------------------------------------------------------------------------------------
void SsfMgr_destroy(SsfMgrRef me)
{
    if(me == NULL)
    {
        return;
    }

    ILog_msg(me->sysLogRef, LOG_LEVEL_DBG, "Destroying %s %s...", me->appName, (me->isDaemonActive) ? "daemon" : "parent");

    SysStateMarking_destroy(me->sysState);
    WatchdogMgr_destroy(me->watchdogMgrRef);

    EvtLoopSyncer_destroy(me->evtLoopSyncerRef);
    EvtLoop_destroy(me->evtLoopRef);

    if(me->isDaemonActive && (me->pidFile != NULL))
    {
        Daemonizer_removePidFile(me->pidFile);
    }
    free(me);
}
//--------------------------------------------------------------------------------------------
void SsfMgr_init(SsfMgrRef me, bool isDaemonized, const char* pidFile, IUnixInitSysRef initSysItfRef, FileContainer_t systemUpFile,
                 FileContainer_t sysWatchdogFile, FileContainer_t brokerPingRequestFile, FileContainer_t brokerPingResponseFile)
{
    assert(me != NULL);
    me->evtLoopLogRef = SysLogger_getLoggerInstance(LOG_NAME_EVTLOOP, true);
    if(isDaemonized)
    {
        assert(pidFile != NULL);
        me->pidFile = pidFile;
        ILog_msg(me->sysLogRef, LOG_LEVEL_DBG, "Will daemonize using pid-file=%s", me->pidFile);
    }
    me->pid = (int)getpid();

    createAndSetupEventLoop(me);

    // Marking System State part:
    if(me->isMarkingEnabled)
    {
        me->initSysItfRef = initSysItfRef;
        me->systemUpFile  = systemUpFile;
        me->sysState      = SysStateMarking_create(me->sysStateLogRef);
    }

    // Watchdog part:
    if(me->isWatchdogEnabled)
    {
        me->systemWatchdogFile     = sysWatchdogFile;
        me->brokerPingReqestFile   = brokerPingRequestFile;
        me->brokerPingResponseFile = brokerPingResponseFile;
        me->watchdogMgrRef         = WatchdogMgr_create(me->wdMgrLogRef, me->pingLogRef,
                                                        me->evtLoopRef, me->evtLoopSyncerRef,
                                                        me->brokerPingReqestFile,
                                                        me->brokerPingResponseFile,
                                                        me->systemWatchdogFile,
                                                        me->watchdogTimeoutMS,
                                                        me->watchdogCheckIntervalMS);
    }

    ILog_msg(me->sysLogRef, LOG_LEVEL_DBG, "%s successfully initialized", me->appName);
}
//--------------------------------------------------------------------------------------------
void SsfMgr_postDaemonizeInit(SsfMgrRef me)
{
    assert(me != NULL);

    // NOTE: put inhere all inits that need to be done after daemonizing

    if(me->isMarkingEnabled)
    {
        SysStateMarking_init(me->sysState, me->evtLoopRef, me->initSysItfRef, me->systemUpFile);
    }
    if(me->isWatchdogEnabled)
    {
        WatchdogMgr_start(me->watchdogMgrRef);
    }
}
//--------------------------------------------------------------------------------------------
bool SsfMgr_daemonize(SsfMgrRef me)
{
    assert(me != NULL);
    assert(me->pidFile != NULL);

    pid_t forkedPid = Daemonizer_daemonize(me->pidFile);
    me->isDaemonActive = (forkedPid == 0);
    if(me->isDaemonActive)
    {
        me->pid = (int)getpid();
        EvtLoop_reinit(me->evtLoopRef);
        ILog_msg(me->sysLogRef, LOG_LEVEL_DBG, "%s daemonized, re-initialized eventloop and pid", me->appName);
    }
    return me->isDaemonActive;
}
//--------------------------------------------------------------------------------------------
void SsfMgr_run(SsfMgrRef me)
{
    assert(me != NULL);
    ILog_msg(me->sysLogRef, LOG_LEVEL_DBG, "%s set up, pid=%d, running...", me->appName, me->pid);
    EvtLoop_run(me->evtLoopRef);
}

