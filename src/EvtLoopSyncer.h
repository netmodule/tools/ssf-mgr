/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * EvtLoopSyncer MODULE
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : EvtLoopSyncer.h
 * CREATION DATE : Aug 30, 2021
 * TARGET        : embedded Linux
 *
 *********************************************************************************************/
#ifndef SRC_EVTLOOPSYNCER_H_
#define SRC_EVTLOOPSYNCER_H_

/** @ingroup SsfManager
 * @defgroup EvtLoopSyncer Event-Loop Synchronizer
 * Provides the necessary functions to trigger the event loop and synchronize data over
 * different modules.
 */

#include <nmapp/types/nm-types.h>
#include <nmapp/logging/ILog.h>
#include <nmapp/event/evt-loop.h>


#ifdef __cplusplus
extern "C" {
#endif


/*********************************************************************************************
 *        GLOBAL DECLARATIONS
 *********************************************************************************************/

typedef struct __EvtLoopSyncerPrivate * EvtLoopSyncerRef;

/** Event-Loop Syncer event data size */
#define EVTLOOP_SYNCER_EVENT_DATA_SIZE  32

/** Event ID definitions */
typedef enum {
    NO_EVENT_ID          = 0,          //!< No event
    SSF_PING_RESPONSE_ID = 1,          //!< Broker ping response
} EvtId_t;

/** Event structure */
typedef struct {
    EvtId_t    id;                     //!< event ID
    uint32_t   dataSize;               //!< size of the event data
    char       data[EVTLOOP_SYNCER_EVENT_DATA_SIZE]; //!< 8-bit character value array
} Event_t;

/**
 * Event-Loop Syncer Callback Function, is called when writing to the event loop syncer pipe.
 *
 * @param pData     The reference to the received data
 * @param dataSize  The size of the received data
 * @param pUsrCtx   The reference to user context (--> will be of type AppFactoryRef)
 */
typedef void (*EvtLoopSyncerCbFn)(const char *pData, uint32_t dataSize, GenericTypeRef pUsrCtx);


/*********************************************************************************************
 *        GLOBAL FUNCTIONS
 *********************************************************************************************/
//--------------------------------------------------------------------------------------------
/** @brief Create Event-Loop Syncer
 * Creates an instance of EvtLoopSyncer
 *
 * @param logRef                    A reference to the logger
 * @param evtLoopRef                A reference to the event-loop instance
 * @param syncCbFn                  A reference to the event-loop syncer event callback function
 * @param syncCbCtx                 A reference to the context provided with as argument in the event-loop syncer event callback function
 * @return The reference to the created instance
 */
EvtLoopSyncerRef EvtLoopSyncer_create(ILogRef logRef, EvtLoopRef evtLoopRef, EvtLoopSyncerCbFn syncCbFn, GenericTypeRef syncCbCtx);
//--------------------------------------------------------------------------------------------
/** @brief Destroy Event-Loop Syncer
 * Destroys an instance of EvtLoopSyncer
 *
 * @param me      A reference to the EvtLoopSyncer instance
 */
void EvtLoopSyncer_destroy(EvtLoopSyncerRef me);
//--------------------------------------------------------------------------------------------
/** @brief Set the sync event callback function
 * Sets the event-loop syncer callback function (function triggered if sync received).
 *
 * NOTE: Be careful when changing the callback and the context there is no handling on
 *       message level, i.e. if you call this function you'll change the settings for
 *       the entire sync pipe.
 *
 * @param me       A reference to the EvtLoopSyncer instance
 * @param newCb    A referecne to the new callback function
 * @param pUsrCtx  A pointer to the user context
 */
void EvtLoopSyncer_setSyncCallbackFunction(EvtLoopSyncerRef me, EvtLoopSyncerCbFn newCb, GenericTypeRef pUsrCtx);
//--------------------------------------------------------------------------------------------
/** @brief Sync Event
 * Syncs an event over the event-loop syncer
 *
 * @param me        A reference to the EvtLoopSyncer instance
 * @param evtId     The ID of the event
 * @param pData     A reference to the data to sync
 * @param dataSize  The data size
 */
void EvtLoopSyncer_syncEvent(EvtLoopSyncerRef me, EvtId_t evtId, const char* pData, uint32_t dataSize);


//--------------------------------------------------------------------------------------------
// FOR Unit Tests Only
//--------------------------------------------------------------------------------------------
void EvtLoopSyncer_enableMockedWrite(EvtLoopSyncerRef me, bool enable);


#ifdef __cplusplus
}
#endif

#endif /* SRC_EVTLOOPSYNCER_H_ */
