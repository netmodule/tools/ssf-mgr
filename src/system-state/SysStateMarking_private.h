/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * SysStateMarking_private MODULE
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : SysStateMarking_private.h
 * CREATION DATE : Aug 30, 2021
 * TARGET        : embedded Linux
 *
 *********************************************************************************************/
#ifndef SRC_SYSSTATEMARKING_PRIVATE_H_
#define SRC_SYSSTATEMARKING_PRIVATE_H_

#include <nmapp/logging/SysLogger.h>
#include <nmapp/sys/IUnixInitSys.h>
#include "SysStateMarking.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef struct __SsfStateMarkingPrivate
{
    ILogRef          logRef;
    IUnixInitSysRef  initSysItf;
    bool             isSystemUp;
    bool             isRebooting;
    FileContainer_t  systemUpFile;
} SsfStateMarkingPrivate_t;


//--------------------------------------------------------------------------------------------
// FOR Unit Tests Only
//--------------------------------------------------------------------------------------------
void onSystemUp(void* calleCtx);
void onSystemPoweroff(void* calleCtx);
void onSystemReboot(void* calleCtx);


#ifdef __cplusplus
}
#endif

#endif /* SRC_SYSSTATEMARKING_PRIVATE_H_ */
