/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * SysStateMarking HEADER
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : SysStateMarking.h
 * CREATION DATE : Aug 26, 2021
 * TARGET        : embedded Linux
 *
 *********************************************************************************************/
#ifndef SRC_SYSSTATEMARKING_H_
#define SRC_SYSSTATEMARKING_H_

/** @ingroup SsfManager
 * @defgroup SysStateMarking System State Marking
 * This module is responsible to mark different states of the system state framework:
 * - mark as up (when fully started up, i.e. systemd reports that everything is started up)
 * - mark as powering down
 * - mark as rebooting
 * .
 *
 */

#include <nmapp/types/nm-types.h>
#include <nmapp/logging/ILog.h>
#include <nmapp/event/evt-loop.h>
#include <nmapp/sys/IUnixInitSys.h>
#include "FileSystem.h"


#ifdef __cplusplus
extern "C" {
#endif


/*********************************************************************************************
 *        GLOBAL DECLARATIONS
 *********************************************************************************************/
typedef struct __SsfStateMarkingPrivate * SysStateMarkingRef;

#define LOG_NAME_INITSYS                  "initSys"
#define LOG_NAME_SYSSTATE                 "systemState"

/*********************************************************************************************
 *        GLOBAL FUNCTIONS
 *********************************************************************************************/
//--------------------------------------------------------------------------------------------
/** @brief Create System State Marking
 * Creates an instance of the System State Marking
 *
 * @param logRef           A reference to the logger to use
 * @return The reference to the created instance
 */
SysStateMarkingRef SysStateMarking_create(ILogRef logRef);
//--------------------------------------------------------------------------------------------
/** @brief Destroy System State Marking
 * Destroys an instance of the System State Marking
 *
 * @param me      A reference to the SysStateMarking instance
 */
void SysStateMarking_destroy(SysStateMarkingRef me);
//--------------------------------------------------------------------------------------------
/** @brief Initialize System State Marking
 * Initializes an instance of the System State Marking
 *
 * @param me               A reference to the SysStateMarking instance
 * @param evtLoopRef       A reference to the event loop
 * @param initSysItfRef    A reference to the unix init-sys interface
 * @param systemUpFile     A handle to the system state up file
 */
void SysStateMarking_init(SysStateMarkingRef me, EvtLoopRef evtLoopRef, IUnixInitSysRef initSysItfRef, FileContainer_t systemUpFile);


#ifdef __cplusplus
}
#endif

#endif /* SRC_SYSSTATEMARKING_H_ */
