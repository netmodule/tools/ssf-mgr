/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * SysStateMarking MODULE
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : SysStateMarking.c
 * CREATION DATE : Aug 26, 2021
 * TARGET        : embedded Linux
 *
 *********************************************************************************************/

#include <stdlib.h>                    // for calloc, free, exit, ...
#include <string.h>                    // for strlen, strcmp, ...

#include <nmapp/exception/exception.h>
#include <nmapp/sys/assertion.h>
#include "SysStateMarking_private.h"

/*********************************************************************************************
 *        LOCALS
 *********************************************************************************************/

const char* dbusSystemdSvc = "org.freedesktop.systemd1";
const char* dbusSystemdPath = "/org/freedesktop/systemd1";
const char* dbusSystemdMgrItf = "org.freedesktop.systemd1.Manager";

const char* dbusSignalSysUp = "StartupFinished";
const char* dbusItemSysUp = "up";
const char* dbusItemSysUpRunning = "running";
const char* dbusSignalShutdown = "UnitNew";
const char* dbusItemPoweroff = "poweroff.target";
const char* dbusItemReboot = "reboot.target";





//--------------------------------------------------------------------------------------------
void markSystemUpState(SysStateMarkingRef me, const char* markString)
{
    if(IFileOperation_isExist(me->systemUpFile.fileOpItf, me->systemUpFile.fileName))
    {
        IFileOperation_open(me->systemUpFile.fileOpItf, me->systemUpFile.fileName, "w");
        IFileOperation_writeToFile(me->systemUpFile.fileOpItf, markString, (uint32_t)strlen(markString));
        IFileOperation_close(me->systemUpFile.fileOpItf);
        return;
    }
    ILog_msg(me->logRef, LOG_LEVEL_INFO, "File %s not present, skipping marking as %s",
                                         me->systemUpFile.fileName, markString);
}
//--------------------------------------------------------------------------------------------
void onSystemUp(void* calleCtx)
{
    SysStateMarkingRef me = (SysStateMarkingRef)calleCtx;
    assert(me != NULL);

    if(me->isSystemUp == false)
    {
        const char  *opState = IUnixInitSys_getOperatingState(me->initSysItf);
        ILog_msg(me->logRef, LOG_LEVEL_DBG, "system state: %s", opState);
        bool isRunning = (strncmp(dbusItemSysUpRunning, opState, strlen(dbusItemSysUpRunning)) == 0);
        const char* stateUpString = "up";
        const char* logStr = isRunning ? "Marking system as up" : "Degraded start-up, marking system as up";
        ILog_msg(me->logRef, LOG_LEVEL_INFO, "%s", logStr);
        me->isSystemUp = true;
        markSystemUpState(me, stateUpString);
    }
}
//--------------------------------------------------------------------------------------------
void onSystemPoweroff(void* calleCtx)
{
    SysStateMarkingRef me = (SysStateMarkingRef)calleCtx;
    assert(me != NULL);

    if(!me->isRebooting)
    {
        ILog_msg(me->logRef, LOG_LEVEL_INFO, "Marking system for powering off");
        const char* statePowerOffString = "powerdown";
        markSystemUpState(me, statePowerOffString);
    }
}
//--------------------------------------------------------------------------------------------
void onSystemReboot(void* calleCtx)
{
    SysStateMarkingRef me = (SysStateMarkingRef)calleCtx;
    assert(me != NULL);

    ILog_msg(me->logRef, LOG_LEVEL_INFO, "Marking system for rebooting");
    const char* stateRebootString = "reboot";
    markSystemUpState(me, stateRebootString);
    me->isRebooting = true;
}



//--------------------------------------------------------------------------------------------
void setupDBusEvents(SysStateMarkingRef me, EvtLoopRef evtLoopRef)
{
    assert(me != NULL);

    // System startup:
    EvtLoop_registerDBusSignal(evtLoopRef, me->initSysItf, dbusSystemdSvc, dbusSystemdPath, dbusSystemdMgrItf,
                               dbusSignalSysUp, dbusItemSysUp, &onSystemUp, me);

    // System shutdown:
    EvtLoop_registerDBusSignal(evtLoopRef, me->initSysItf, dbusSystemdSvc, dbusSystemdPath, dbusSystemdMgrItf,
                               dbusSignalShutdown, dbusItemPoweroff, &onSystemPoweroff, me);
    EvtLoop_registerDBusSignal(evtLoopRef, me->initSysItf, dbusSystemdSvc, dbusSystemdPath, dbusSystemdMgrItf,
                               dbusSignalShutdown, dbusItemReboot, &onSystemReboot, me);

    IUnixInitSys_activateDBusListener(me->initSysItf);

    ILog_msg(me->logRef, LOG_LEVEL_DBG, "Registered all dbus events to the event loop");
}



/*********************************************************************************************
 *        GLOBALS
 *********************************************************************************************/
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
SysStateMarkingRef SysStateMarking_create(ILogRef logRef)
{
    SysStateMarkingRef me = (SysStateMarkingRef)calloc(1, sizeof(SsfStateMarkingPrivate_t));
    if(me == NULL)
    {
        Throw(EX_CREATE, "Could not create SSF marking instance");
    }
    me->logRef = logRef;
    me->isRebooting = false;
    me->isSystemUp = false;
    ILog_msg(me->logRef, LOG_LEVEL_DBG, "SysStateMarking created");
    return me;
}
//--------------------------------------------------------------------------------------------
void SysStateMarking_destroy(SysStateMarkingRef me)
{
    if(me == NULL)
    {
        return;
    }

    ILog_msg(me->logRef, LOG_LEVEL_DBG, "Destroying SysStateMarking...");
    free(me);
}
//--------------------------------------------------------------------------------------------
void SysStateMarking_init(SysStateMarkingRef me, EvtLoopRef evtLoop, IUnixInitSysRef initSysItfRef, FileContainer_t systemUpFile)
{
    assert(me != NULL);
    assert(initSysItfRef != NULL);
    assert(systemUpFile.fileOpItf != NULL);
    assert(systemUpFile.fileName  != NULL);

    me->initSysItf = initSysItfRef;
    me->systemUpFile = systemUpFile;

    setupDBusEvents(me, evtLoop);

    ILog_msg(me->logRef, LOG_LEVEL_DBG, "SysStateMarking successfully initialized");
}
