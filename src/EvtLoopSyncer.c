/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * EvtLoopSyncer MODULE
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : EvtLoopSyncer.c
 * CREATION DATE : Aug 30, 2021
 * TARGET        : <Target: generic,embedded Linux,...>
 *
 *********************************************************************************************/

#include <stdlib.h>                    // for calloc, free, exit, ...
#include <unistd.h>                    // for pipe2
#include <fcntl.h>                     // for pipe2 O_NONBLOCK
#include <errno.h>                     // for error numbers
#include <string.h>                    // for strerror, memcpy, ...

#include <nmapp/exception/exception.h>
#include <nmapp/sys/assertion.h>
#include "EvtLoopSyncer.h"


/*********************************************************************************************
 *        LOCALS
 *********************************************************************************************/

#define PIPE_BUF_SIZE   128

enum
{
    PIPE_RD_IDX     = 0,
    PIPE_WR_IDX     = 1,
    PIPE_NBR_OF_FDS = 2
};


typedef void (*WriteSyncEvtFn)(int fd,  const void *buffer, size_t size);


typedef struct __EvtLoopSyncerPrivate
{
    EvtLoopRef         evtLoopRef;
    ILogRef            logRef;
    int                pipeFds[PIPE_NBR_OF_FDS];
    uint32_t           pipeEventSize;
    EvtLoopSyncerCbFn  syncEvtCallbackFn;
    GenericTypeRef     userCtxRef;
    WriteSyncEvtFn     writeSyncEvtFn;
} EvtLoopSyncerPrivate_t;



//--------------------------------------------------------------------------------------------
static void evtLoopTriggerCallback(void *pCtx)
{
    EvtLoopSyncerRef me = (EvtLoopSyncerRef)pCtx;
    static char readBuffer[PIPE_BUF_SIZE];
    static size_t prevReadSize = 0;
    static size_t eventSize = sizeof(Event_t);

    ILog_msg(me->logRef, LOG_LEVEL_DBG, "event loop sync triggered");
    memset(&readBuffer[0], 0, PIPE_BUF_SIZE);
    size_t readingSize = (eventSize - prevReadSize);
    size_t readSize = (size_t)read(me->pipeFds[PIPE_RD_IDX], &readBuffer[prevReadSize], readingSize);
    if (readSize < 0)
    {
        Throw(EX_READ, "Failed reading pipe: %s", strerror(errno));
    }
    if (readSize != readingSize) // not all data received, need to wait on rest of the data
    {
        ILog_msg(me->logRef, LOG_LEVEL_DBG, "not all data read (%d of %d - evtSize=%d)",
                                            (int)readSize, (int)readingSize, (int)eventSize);
        prevReadSize = readSize;
        return;
    }
    prevReadSize = 0;

    if(me->syncEvtCallbackFn != NULL)
    {
        ILog_msg(me->logRef, LOG_LEVEL_DBG, "calling registered sync event callback...");
        me->syncEvtCallbackFn((const char*)&readBuffer[0], (uint32_t)eventSize, me->userCtxRef);
    }
}
//--------------------------------------------------------------------------------------------
void setupSyncPipe(EvtLoopSyncerRef me)
{
    assert(me != NULL);
    if(me->syncEvtCallbackFn == NULL)
    {
        return;
    }

    if(pipe(me->pipeFds) != 0)
    {
        Throw(EX_CREATE, "Cannot create sync pipe for event-loop syncer: %d - %s", errno, strerror(errno));
    }
    int pipeFlags = fcntl(me->pipeFds[PIPE_RD_IDX], F_GETFL);
    if(pipeFlags < 0)
    {
        Throw(EX_READ, "Cannot read pipe flags: %d - %s", errno, strerror(errno));
    }
    pipeFlags |= O_NONBLOCK;
    if(fcntl(me->pipeFds[PIPE_RD_IDX], F_SETFL) != 0)
    {
        Throw(EX_WRITE, "Cannot write pipe flags: %d - %s", errno, strerror(errno));
    }

    ILog_msg(me->logRef, LOG_LEVEL_DBG, "EvtLoopSyncer file descriptors: fdRead=%d, fdWrite=%d",
                                         me->pipeFds[PIPE_RD_IDX], me->pipeFds[PIPE_WR_IDX]);
    EvtLoop_registerEPollEvent(me->evtLoopRef, me->pipeFds[PIPE_RD_IDX], &evtLoopTriggerCallback, true, me);
}
//--------------------------------------------------------------------------------------------
void destroySyncPipe(EvtLoopSyncerRef me)
{
    if(me->syncEvtCallbackFn == NULL)
    {
        return;
    }
    close(me->pipeFds[PIPE_RD_IDX]);
    close(me->pipeFds[PIPE_WR_IDX]);
    me->pipeFds[PIPE_RD_IDX] = -1;
    me->pipeFds[PIPE_WR_IDX] = -1;
}

//--------------------------------------------------------------------------------------------
void mockWriteSyncEvent(int fd,  const void *buffer, size_t size)
{
    assert(buffer != NULL);
}
//--------------------------------------------------------------------------------------------
void writeSyncEvent(int fd,  const void *buffer, size_t size)
{
    errno = 0;
    ssize_t bytesWritten = write(fd, buffer, size);
    if(bytesWritten != size)
    {
        Throw(EX_WRITE, "Could not write sync event: %s", strerror(errno));
    }
}

/*********************************************************************************************
 *        GLOBALS
 *********************************************************************************************/
//--------------------------------------------------------------------------------------------
EvtLoopSyncerRef EvtLoopSyncer_create(ILogRef logRef, EvtLoopRef evtLoopRef, EvtLoopSyncerCbFn syncCbFn, GenericTypeRef pUsrCtx)
{
    assert(evtLoopRef != NULL);

    EvtLoopSyncerRef me = (EvtLoopSyncerRef)calloc(1, sizeof(EvtLoopSyncerPrivate_t));
    if(me == NULL)
    {
        Throw(EX_CREATE, "Could not create EvtLoop Syncer instance");
    }
    me->logRef            = logRef;
    me->evtLoopRef        = evtLoopRef;
    me->syncEvtCallbackFn = syncCbFn;
    me->writeSyncEvtFn    = &writeSyncEvent;
    me->pipeEventSize     = PIPE_BUF_SIZE;
    me->userCtxRef        = pUsrCtx;
    setupSyncPipe(me);
    ILog_msg(me->logRef, LOG_LEVEL_DBG, "EvtLoopSyncer created and ready");
    return me;
}
//--------------------------------------------------------------------------------------------
void EvtLoopSyncer_destroy(EvtLoopSyncerRef me)
{
    if(me == NULL)
    {
        return;
    }
    destroySyncPipe(me);
    free(me);
}
//--------------------------------------------------------------------------------------------
void EvtLoopSyncer_setSyncCallbackFunction(EvtLoopSyncerRef me, EvtLoopSyncerCbFn newCb, GenericTypeRef pUsrCtx)
{
    assert(me != NULL);
    bool isSetupRequired  = (me->syncEvtCallbackFn == NULL) ? true : false;
    me->syncEvtCallbackFn = newCb;
    me->userCtxRef        = pUsrCtx;
    if(isSetupRequired)
    {
        setupSyncPipe(me);
    }
    ILog_msg(me->logRef, LOG_LEVEL_INFO, "Successfully set EvtLoopSyncer callback function");
}
//--------------------------------------------------------------------------------------------
void EvtLoopSyncer_syncEvent(EvtLoopSyncerRef me, EvtId_t evtId, const char* pData, uint32_t dataSize)
{
    assert(me != NULL);
    if(dataSize > EVTLOOP_SYNCER_EVENT_DATA_SIZE)
    {
        Throw(EX_LIMIT_CHECK, "Event size (%u) too high, MAX = %u", dataSize, me->pipeEventSize);
    }
    Event_t syncEvt = {.id = evtId, .dataSize = dataSize, .data[0] = '\0'};
    memcpy(&syncEvt.data[0], pData, dataSize);
    uint32_t eventSize = sizeof(syncEvt);
    // security check if something is changed on the pipe/event structure
    if(eventSize > me->pipeEventSize)
    {
        Throw(EX_LIMIT_CHECK, "Event size (%u) too high, MAX = %u", dataSize, me->pipeEventSize);
    }
    me->writeSyncEvtFn(me->pipeFds[PIPE_WR_IDX], &syncEvt, eventSize);
    ILog_msg(me->logRef, LOG_LEVEL_DBG, "Sync event (id=%u) sent (eventSize=%u)", syncEvt.id, (unsigned int)eventSize);
}




//--------------------------------------------------------------------------------------------
// FOR Unit Tests Only
//--------------------------------------------------------------------------------------------
void EvtLoopSyncer_enableMockedWrite(EvtLoopSyncerRef me, bool enable)
{
    assert(me != NULL);
    me->writeSyncEvtFn = (enable) ? &mockWriteSyncEvent : &writeSyncEvent;
}
