/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * ssf-mgr HEADER
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : ssf-mgr.h
 * CREATION DATE : Aug 23, 2021
 * TARGET        : embedded Linux
 *
 *********************************************************************************************/
#ifndef _SSF_MGR_H_
#define _SSF_MGR_H_

/** @defgroup SsfManager System State Framework Manager
 * This application contains functionality of the system state framework and may act as daemon.
 * The features can be enabled or disabled over the command line arguments.
 */

#include <nmapp/types/nm-types.h>
#include <nmapp/logging/SysLogger.h>
#include <nmapp/event/evt-loop.h>
#include <nmapp/sys/IUnixInitSys.h>
#include "EvtLoopSyncer.h"
#include "FileSystem.h"
#include "system-state/SysStateMarking.h"
#include "watchdog/WatchdogMgr.h"


#ifdef __cplusplus
extern "C" {
#endif


/*********************************************************************************************
 *        GLOBAL DECLARATIONS
 *********************************************************************************************/

#define LOG_NAME_EVTLOOP     "evtloop"
#define PIPE_BUF_SIZE        128

typedef struct __SsfMgrPrivate * SsfMgrRef;

typedef struct __SsfMgrPrivate
{
    const char         *appName;
    bool                isMarkingEnabled;
    bool                isWatchdogEnabled;
    bool                isDaemonActive;
    const char         *pidFile;
    int                 pid;
    uint32_t            watchdogTimeoutMS;
    uint32_t            watchdogCheckIntervalMS;
    ILogRef             sysLogRef;
    ILogRef             evtLoopLogRef;
    EvtLoopRef          evtLoopRef;
    EvtLoopSyncerRef    evtLoopSyncerRef;

    ILogRef             sysStateLogRef;
    IUnixInitSysRef     initSysItfRef;
    FileContainer_t     systemUpFile;
    SysStateMarkingRef  sysState;

    ILogRef             wdMgrLogRef;
    ILogRef             pingLogRef;
    FileContainer_t     systemWatchdogFile;
    FileContainer_t     brokerPingReqestFile;
    FileContainer_t     brokerPingResponseFile;
    WatchdogMgrRef      watchdogMgrRef;
} SsfMgrPrivate_t;



/*********************************************************************************************
 *        GLOBAL FUNCTIONS
 *********************************************************************************************/
//--------------------------------------------------------------------------------------------
/**
 * @brief Create SystemStateFramework Manager
 * Creates an instance of the system state framework manager
 *
 * @param appName               The name of the application
 * @param logRef                A reference to the logger to use
 * @param sysStateLogRef        A reference to the system state logger
 * @param watchdogMgrLogRef     A reference to the watchdog manager logger
 * @param brokerPingerLogRef    A reference to the broker pinger logger
 * @param isMarkingUp           true to mark the system as up when up; false to skip marking
 * @param isSupervising         true to supervise SSF modules and handle watchdog; false otherwise
 * @param wdTimeoutMS           The watchdog timeout [ms] to configure
 * @param wdChkIntervalMS       The watchdog check interval [ms]
 * @return The reference to the created instance
 */
SsfMgrRef SsfMgr_create(const char* appName, ILogRef logRef,
                        ILogRef sysStateLogRef,
                        ILogRef watchdogMgrLogRef, ILogRef brokerPingerLogRef,
                        bool isMarkingUp, bool isSupervising, uint32_t wdTimeoutMS, uint32_t wdChkIntervalMS);
//--------------------------------------------------------------------------------------------
/**
 * @brief Destroy SystemStateFramework Manager
 * Destroys an instance of the system state framework manager
 *
 * @param me      A reference to the SsfMgr instance
 */
void SsfMgr_destroy(SsfMgrRef me);
//--------------------------------------------------------------------------------------------
/**
 * @brief Init SystemStateFramework Manager
 * Initializes an instance of the system state framework manager
 *
 * @param me                     A reference to the SsfMgr instance
 * @param isDaemonized           true = daemonize the app, false = run in foreground
 * @param pidFile                The pid file pathname to create when isDaemonized=true
 * @param initSysItfRef          A reference to the unix init-sys interface
 * @param systemUpFile           A handle to the system state up file
 * @param sysWatchdogFile        A handle to the system watchdog file
 * @param brokerPingRequestFile  A handle to the broker ping request  file
 * @param brokerPingResponseFile A handle to the broker ping response file
 */
void SsfMgr_init(SsfMgrRef me, bool isDaemonized, const char* pidFile, IUnixInitSysRef initSysItfRef, FileContainer_t systemUpFile,
                 FileContainer_t sysWatchdogFile, FileContainer_t brokerPingRequestFile, FileContainer_t brokerPingResponseFile);
//--------------------------------------------------------------------------------------------
/**
 * @brief Post-Daemonize Init of SystemStateFramework Manager
 * Initializes modules, elements after daemonizing.
 *
 * @param me              A reference to the SsfMgr instance
 */
void SsfMgr_postDaemonizeInit(SsfMgrRef me);
//--------------------------------------------------------------------------------------------
/**
 * @brief Daemonize SystemStateFramework Manager
 * Daemonizes the application by using libnm daemonizer. The return value specifies
 * whether child or parent is active
 *
 * @param me              A reference to the SsfMgr instance
 * @return true = daemon (= child) active; false = parent (=foreground) active
 */
bool SsfMgr_daemonize(SsfMgrRef me);
//--------------------------------------------------------------------------------------------
/**
 * @brief Run SystemStateFramework Manager
 * Runs the application by entering the event loop
 *
 * @param me              A reference to the SsfMgr instance
 */
void SsfMgr_run(SsfMgrRef me);


#ifdef __cplusplus
}
#endif

#endif /* _SSF_MGR_H_ */
