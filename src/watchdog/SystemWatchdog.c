/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * SystemWatchdog MODULE
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : SystemWatchdog.c
 * CREATION DATE : Aug 31, 2021
 * TARGET        : <Target: generic,embedded Linux,...>
 *
 *********************************************************************************************/

#include <sys/ioctl.h>
#include <sys/types.h>          // for ssize_t
#include <unistd.h>             // for write
#include <string.h>             // for strncpy, strerror
#include <linux/watchdog.h>

#include <nmapp/exception/exception.h>
#include <nmapp/sys/assertion.h>
#include "SystemWatchdog_private.h"

/*********************************************************************************************
 *        LOCALS
 *********************************************************************************************/

// for singleton:
SystemWatchdog_t  SysWatchdog;
static SystemWatchdogRef localMe = NULL;


// FOR Unit Tests Only:
int  SysWdMockWdTimeoutS;
int  SysWdMockWdStatus;

//--------------------------------------------------------------------------------------------
static void setWatchdogTimeout(SystemWatchdogRef me, uint32_t timeoutS)
{
    assert(me->fdWd != -1);
    if (ioctl(me->fdWd, WDIOC_SETTIMEOUT, &timeoutS) != 0)
    {
        Throw(EX_READ, "Failed writing watchdog timeout=%us: %s", (unsigned int)timeoutS, strerror(errno));
    }
}
//--------------------------------------------------------------------------------------------
static void setWatchdogTimeoutMock(SystemWatchdogRef me, uint32_t timeoutS)
{
    assert(me->fdWd != -1);
    // use IFileOperation in mock
    SysWdMockWdTimeoutS = (int)timeoutS;
}


//--------------------------------------------------------------------------------------------
static int getWatchdogTimeout(SystemWatchdogRef me)
{
    assert(me->fdWd != -1);
    int intervalS = 0;
    if(ioctl(me->fdWd, WDIOC_GETTIMEOUT, &intervalS) < 0)
    {
        Throw(EX_READ, "Failed reading watchdog interval: %s", strerror(errno));
    }
    return intervalS;
}
//--------------------------------------------------------------------------------------------
static int getWatchdogTimeoutMock(SystemWatchdogRef me)
{
    assert(me->fdWd != -1);
    return (int)SysWdMockWdTimeoutS;
}


//--------------------------------------------------------------------------------------------
static int getBootStatus(SystemWatchdogRef me)
{
    assert(me->fdWd != -1);
    int bootStatus = 0;
    if(ioctl(me->fdWd, WDIOC_GETBOOTSTATUS, &bootStatus) < 0)
    {
        Throw(EX_READ, "Failed reading watchdog boot status: %s", strerror(errno));
    }
    return bootStatus;
}
//--------------------------------------------------------------------------------------------
static int getBootStatusMock(SystemWatchdogRef me)
{
    assert(me->fdWd != -1);
    return (int)SysWdMockWdStatus;
}


//--------------------------------------------------------------------------------------------
static void writeToWatchdogDevice(SystemWatchdogRef me, const char* data, uint32_t size)
{
    assert(me->fdWd != -1);
    write(me->fdWd, data, size);
}
//--------------------------------------------------------------------------------------------
static void writeToWatchdogDeviceMock(SystemWatchdogRef me, const char* data, uint32_t size)
{
    assert(me->fdWd != -1);
    // use IFileOperation in mock
    IFileOperation_writeToFile(me->fileContainer.fileOpItf, data, (uint32_t)size);
}



/*********************************************************************************************
 *        GLOBALS
 *********************************************************************************************/
//--------------------------------------------------------------------------------------------
SystemWatchdogRef SystemWatchdog_create(ILogRef logRef, FileContainer_t fileContainer, uint32_t watchdogTimeoutMS, bool useWdBypass)
{
    if(localMe != NULL)
    {
        ILog_msg(localMe->logRef, LOG_LEVEL_WARN, "Singleton of SystemWatchdog is created multiple times");
        return localMe;
    }

    memset(&SysWatchdog, 0, sizeof(SysWatchdog));
    localMe = (SystemWatchdogRef)&SysWatchdog;
    localMe->fdWd            = -1;
    localMe->logRef          = logRef;
    localMe->fileContainer   = fileContainer;
    localMe->wdTimeoutS      = (uint32_t)(((float64_t)watchdogTimeoutMS / MILLISECONDS_PER_SECOND) + 0.5);
    localMe->isMockEnabled   = useWdBypass;
    localMe->getTimeoutFn    = &getWatchdogTimeout;
    localMe->setTimeoutFn    = &setWatchdogTimeout;
    localMe->getBootStatusFn = &getBootStatus;
    localMe->writeWatchdogFn = &writeToWatchdogDevice;
    ILog_msg(localMe->logRef, LOG_LEVEL_DBG, "Created SystemWatchdog");
    if(localMe->isMockEnabled)
    {
        SystemWatchdog_enableMock(localMe, true);
    }
    return localMe;
}
//--------------------------------------------------------------------------------------------
void SystemWatchdog_destroy(SystemWatchdogRef me)
{
    if(localMe == NULL)
    {
        return;
    }
    if(IFileOperation_isFileOpen(me->fileContainer.fileOpItf))
    {
        me->writeWatchdogFn(me, "V", (uint32_t)1);
        IFileOperation_close(me->fileContainer.fileOpItf);
    }
    if(me->isMockEnabled)
    {
        SystemWatchdog_enableMock(me, false);
    }
    memset(localMe, 0, sizeof(SysWatchdog));
    me->fdWd = -1;
    localMe = NULL;
}
//--------------------------------------------------------------------------------------------
void SystemWatchdog_init(SystemWatchdogRef me)
{
    assert(me != NULL);

    if(!IFileOperation_isExist(me->fileContainer.fileOpItf, me->fileContainer.fileName))
    {
        Throw(EX_MISSING, "Missing system watchdog '%s'", me->fileContainer.fileName);
    }
    if(!IFileOperation_isFileOpen(me->fileContainer.fileOpItf))
    {
        IFileOperation_open(me->fileContainer.fileOpItf, me->fileContainer.fileName, "r+");
    }
    me->fdWd = IFileOperation_getFiledescriptor(me->fileContainer.fileOpItf);

    uint32_t actualTimeoutS = (uint32_t)me->getTimeoutFn(me);
    if(me->wdTimeoutS != actualTimeoutS)
    {
        me->setTimeoutFn(me, me->wdTimeoutS);
        ILog_msg(me->logRef, LOG_LEVEL_DBG, "Changd watchdog timeout: %us -> %us", (unsigned int)actualTimeoutS,
                                                                                   (unsigned int)me->wdTimeoutS);
        actualTimeoutS = (uint32_t)me->getTimeoutFn(me);
        ILog_msg(me->logRef, LOG_LEVEL_DBG, "PMIC watchdog timeout (read back): %us", (unsigned int)actualTimeoutS);
    }
    me->wdTimeoutS = actualTimeoutS;
    ILog_msg(me->logRef, LOG_LEVEL_INFO, "SystemWatchdog initialized with timeout=%us", (unsigned int)me->wdTimeoutS);
}
//--------------------------------------------------------------------------------------------
void SystemWatchdog_feed(SystemWatchdogRef me)
{
    assert(me != NULL);
    assert(me->fdWd != -1);
    me->writeWatchdogFn(me, "w", (uint32_t)1);
}


//--------------------------------------------------------------------------------------------
// FOR Unit Tests Only
//--------------------------------------------------------------------------------------------
void SystemWatchdog_enableMock(SystemWatchdogRef me, bool enable)
{
    assert(me != NULL);
    me->getTimeoutFn    = (enable) ? &getWatchdogTimeoutMock    : &getWatchdogTimeout;
    me->setTimeoutFn    = (enable) ? &setWatchdogTimeoutMock    : &setWatchdogTimeout;
    me->getBootStatusFn = (enable) ? &getBootStatusMock         : &getBootStatus;
    me->writeWatchdogFn = (enable) ? &writeToWatchdogDeviceMock : &writeToWatchdogDevice;
}
