/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * WatchdogMgr_private MODULE
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : WatchdogMgr_private.h
 * CREATION DATE : Aug 30, 2021
 * TARGET        : embedded Linux
 *
 *********************************************************************************************/
#ifndef SRC_WATCHDOGMGR_PRIVATE_H_
#define SRC_WATCHDOGMGR_PRIVATE_H_


#include "WatchdogMgr.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct __BrokerCheck
{
    ILogRef          logRef;
    BrokerPingerRef  pingerRef;
    FileContainer_t  pingRequest;
    FileContainer_t  pingResponse;
} BrokerCheck_t;

typedef struct __WatchdogMgrPrivate
{
    ILogRef           logRef;
    EvtLoopRef        evtLoopRef;
    EvtLoopSyncerRef  syncerRef;
    EvtLoopTimerRef   checkTimerRef;
    uint32_t          checkIntervalMS;
    uint32_t          watchdogTimeoutMS;

    BrokerCheck_t     brokerCheck;

    SystemWatchdogRef sysWatchdog;
} WatchdogMgrPrivate_t;


//--------------------------------------------------------------------------------------------
// FOR Unit Tests Only
//--------------------------------------------------------------------------------------------
void checkTimerElapsed(void* calleCtx);
void brokerResponseTimedout(void* calleCtx);

#ifdef __cplusplus
}
#endif

#endif /* SRC_WATCHDOGMGR_PRIVATE_H_ */
