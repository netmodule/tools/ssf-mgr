/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * BrokerPinger_private MODULE
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : BrokerPinger_private.h
 * CREATION DATE : Aug 30, 2021
 * TARGET        : embedded Linux
 *
 *********************************************************************************************/
#ifndef SRC_BROKERPINGER_PRIVATE_H_
#define SRC_BROKERPINGER_PRIVATE_H_

#include "BrokerPinger.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct __BrokerPingerPrivate
{
    ILogRef          logRef;
    EvtLoopRef       evtLoopRef;
    EvtLoopTimerRef  pingResonseTimerRef;
    EvtLoopSyncerRef resultSyncerRef;
    FileContainer_t  pingRequest;
    FileContainer_t  pingResponse;
    uint32_t         pingValue;
    uint32_t         responseReadDelayMS;
} BrokerPingerPrivate_t;




//--------------------------------------------------------------------------------------------
// FOR Unit Tests Only
//--------------------------------------------------------------------------------------------
void pingResponseDelayTimerElapsed(void* calleCtx);

#ifdef __cplusplus
}
#endif

#endif /* SRC_BROKERPINGER_PRIVATE_H_ */
