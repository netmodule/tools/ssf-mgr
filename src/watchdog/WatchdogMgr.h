/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * WatchdogMgr MODULE
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : WatchdogMgr.h
 * CREATION DATE : Aug 30, 2021
 * TARGET        : embedded Linux
 *
 *********************************************************************************************/
#ifndef SRC_WATCHDOGMGR_H_
#define SRC_WATCHDOGMGR_H_

/** @ingroup SsfManager
 * @defgroup WatchdogMgr Watchdog Manager
 * The watchdog manager feeds the watchdog when the SSF works as expected and lets the
 * watchdog starve otherwise.
 */

#include <nmapp/types/nm-types.h>
#include <nmapp/logging/ILog.h>
#include <nmapp/event/evt-loop.h>
#include "../FileSystem.h"
#include "BrokerPinger.h"
#include "SystemWatchdog.h"

#ifdef __cplusplus
extern "C" {
#endif


/*********************************************************************************************
 *        GLOBAL DECLARATIONS
 *********************************************************************************************/

typedef struct __WatchdogMgrPrivate * WatchdogMgrRef;

#define LOG_NAME_WD_MGR               "watchdogMgr"

#define DEFAULT_WD_TIMEOUT_MS         8000
#define DEFAULT_WD_CHECK_INTERVAL_MS  4000


/*********************************************************************************************
 *        GLOBAL FUNCTIONS
 *********************************************************************************************/
//--------------------------------------------------------------------------------------------
/** @brief Create Watchdog Manager
 * Creates an instance of the WatchdogMgr
 *
 * @param logRef                    A reference to the logger
 * @param brokerPingerLogRef        A reference to the broker pinger logger
 * @param evtLoopRef                A reference to the event-loop
 * @param syncerRef                 A reference to the event-loop syncer
 * @param brokerRequestContainer    A file container for the broker ping request
 * @param brokerResponseContainer   A file container for the broker ping response
 * @param sysWatchdogContainer      A file container for the system watchdog
 * @param watchdogTimeoutMS         The watchdog timeout [ms] to configure
 * @param checkIntervalMS           The watchdog check interval [ms]
 * @return The reference to the created instance
 */
WatchdogMgrRef WatchdogMgr_create(ILogRef logRef, ILogRef brokerPingerLogRef,
                                  EvtLoopRef evtLoopRef, EvtLoopSyncerRef syncerRef,
                                  FileContainer_t brokerRequestContainer, FileContainer_t brokerResponseContainer,
                                  FileContainer_t sysWatchdogContainer,
                                  uint32_t watchdogTimeoutMS, uint32_t checkIntervalMS);
//--------------------------------------------------------------------------------------------
/** @brief Destroy Watchdog Manager
 * Destroys an instance of the WatchdogMgr
 *
 * @param me      A reference to the WatchdogMgr instance
 */
void WatchdogMgr_destroy(WatchdogMgrRef me);
//--------------------------------------------------------------------------------------------
/** @brief Start Watchdog Manager
 * Starts the referred WatchdogMgr
 *
 * @param me      A reference to the WatchdogMgr instance
 */
void WatchdogMgr_start(WatchdogMgrRef me);
//--------------------------------------------------------------------------------------------
/** @brief Analyze Ping Status
 * Analyzes all the ping status. This function might be triggered when receiving a
 * status update over the EvtLoopSyncer.
 *
 * @param pData     A reference to the sync data
 * @param dataSize  The size of the sync data
 * @param me        A reference to the WatchdogMgr instance
 */
void WatchdogMgr_analyzePingStatus(const char *pData, uint32_t dataSize, WatchdogMgrRef me);



#ifdef __cplusplus
}
#endif

#endif /* SRC_WATCHDOGMGR_H_ */
