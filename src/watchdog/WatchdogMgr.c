/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * WatchdogMgr MODULE
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : WatchdogMgr.c
 * CREATION DATE : Aug 30, 2021
 * TARGET        : embedded Linux
 *
 *********************************************************************************************/


#include <stdlib.h>                    // for calloc, free, exit, ...
#include <errno.h>                     // for error numbers
#include <string.h>                    // for strerror

#include <nmapp/exception/exception.h>
#include <nmapp/sys/assertion.h>
#include "WatchdogMgr_private.h"

/*********************************************************************************************
 *        LOCALS
 *********************************************************************************************/


//--------------------------------------------------------------------------------------------
void checkTimerElapsed(void* calleCtx)
{
    WatchdogMgrRef me = (WatchdogMgrRef)calleCtx;
    assert(me != NULL);
    ILog_msg(me->logRef, LOG_LEVEL_DBG, "check timer elapsed");
    EvtLoop_stopTimer(me->checkTimerRef);

    BrokerPinger_ping(me->brokerCheck.pingerRef);

    // Restart timer if periodic
    if(EvtLoop_isTimerPeriodic(me->checkTimerRef))
    {
        EvtLoop_startTimer(me->checkTimerRef);
    }
}


/*********************************************************************************************
 *        GLOBALS
 *********************************************************************************************/
//--------------------------------------------------------------------------------------------
WatchdogMgrRef WatchdogMgr_create(ILogRef logRef, ILogRef brokerPingerLogRef,
                                  EvtLoopRef evtLoopRef, EvtLoopSyncerRef syncerRef,
                                  FileContainer_t brokerRequestContainer, FileContainer_t brokerResponseContainer,
                                  FileContainer_t sysWatchdogContainer,
                                  uint32_t watchdogTimeoutMS, uint32_t checkIntervalMS)
{
    assert(evtLoopRef != NULL);
    assert(syncerRef != NULL);
    assert(brokerRequestContainer.fileOpItf != NULL);
    assert(brokerRequestContainer.fileName  != NULL);
    assert(brokerResponseContainer.fileOpItf != NULL);
    assert(brokerResponseContainer.fileName  != NULL);
    assert(sysWatchdogContainer.fileOpItf != NULL);
    assert(sysWatchdogContainer.fileName  != NULL);

    WatchdogMgrRef me = (WatchdogMgrRef)calloc(1, sizeof(WatchdogMgrPrivate_t));
    if(me == NULL)
    {
        Throw(EX_CREATE, "Could not create watchdog manager instance");
    }
    me->logRef     = logRef;
    me->evtLoopRef = evtLoopRef;
    me->syncerRef  = syncerRef;
    me->watchdogTimeoutMS = watchdogTimeoutMS;
    me->checkIntervalMS   = checkIntervalMS;

    me->sysWatchdog = SystemWatchdog_create(me->logRef, sysWatchdogContainer, me->watchdogTimeoutMS, false);
    SystemWatchdog_init(me->sysWatchdog);

    me->brokerCheck.pingRequest  = brokerRequestContainer;
    me->brokerCheck.pingResponse = brokerResponseContainer;
    me->brokerCheck.pingerRef    = BrokerPinger_create(me->evtLoopRef, me->syncerRef, brokerPingerLogRef,
                                                       me->brokerCheck.pingRequest, me->brokerCheck.pingResponse);

    me->checkTimerRef   = EvtLoop_createAndRegisterTimer(me->evtLoopRef, &checkTimerElapsed,
                                                         me->checkIntervalMS, true, me);
    ILog_msg(me->logRef, LOG_LEVEL_DBG, "WatchdogMgr successfully created");
    return me;
}
//--------------------------------------------------------------------------------------------
void WatchdogMgr_destroy(WatchdogMgrRef me)
{
    if(me == NULL)
    {
        return;
    }
    EvtLoop_destroyTimer(me->checkTimerRef);
    BrokerPinger_destroy(me->brokerCheck.pingerRef);
    me->brokerCheck.pingerRef = NULL;
    SystemWatchdog_destroy(me->sysWatchdog);
    free(me);
}
//--------------------------------------------------------------------------------------------
void WatchdogMgr_start(WatchdogMgrRef me)
{
    assert(me != NULL);
    ILog_msg(me->logRef, LOG_LEVEL_INFO, "Starting watchdog supervision (check interval=%ums)", me->checkIntervalMS);
    EvtLoop_startTimer(me->checkTimerRef);
}
//--------------------------------------------------------------------------------------------
void WatchdogMgr_analyzePingStatus(const char *pData, uint32_t dataSize, WatchdogMgrRef me)
{
    assert(pData != NULL);
    assert(me != NULL);

    // Valid ping response:
    if(strncmp((const char*)PING_STATUS_OK, pData, EVTLOOP_SYNCER_EVENT_DATA_SIZE) == 0)
    {
        ILog_msg(me->logRef, LOG_LEVEL_DBG, "Broker ping status = %s, feeding watchdog...", PING_STATUS_OK);
        SystemWatchdog_feed(me->sysWatchdog);
        return;
    }

    // Invalid ping response:
    if(strncmp((const char*)PING_STATUS_NOK, pData, EVTLOOP_SYNCER_EVENT_DATA_SIZE) == 0)
    {
        ILog_msg(me->logRef, LOG_LEVEL_ERR, "Invalid broker ping status, starving watchdog and stopping check timer...");
        EvtLoop_stopTimer(me->checkTimerRef);
        return;
    }

    ILog_msg(me->logRef, LOG_LEVEL_WARN, "Unsupported broker ping status: %s", pData);
}

