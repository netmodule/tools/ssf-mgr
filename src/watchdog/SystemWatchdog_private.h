/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * SystemWatchdog_private MODULE
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : SystemWatchdog_private.h
 * CREATION DATE : Aug 31, 2021
 * TARGET        : embedded Linux
 *
 *********************************************************************************************/
#ifndef SRC_WATCHDOG_SYSTEMWATCHDOG_PRIVATE_H_
#define SRC_WATCHDOG_SYSTEMWATCHDOG_PRIVATE_H_

#include "SystemWatchdog.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef int  (*getWatchdogTimeoutFn)(SystemWatchdogRef me);
typedef int  (*getWatchdogBootStatusFn)(SystemWatchdogRef me);
typedef void (*setWatchdogTimeoutFn)(SystemWatchdogRef me, uint32_t newTimeoutS);
typedef void (*writeWatchdogFn)(SystemWatchdogRef me, const char* data, uint32_t size);

typedef struct __SystemWatchdog
{
    bool                    isMockEnabled;
    bool                    isLastBootReasonWdReset;
    int                     fdWd;
    uint32_t                wdTimeoutS;
    ILogRef                 logRef;
    FileContainer_t         fileContainer;
    getWatchdogTimeoutFn    getTimeoutFn;
    setWatchdogTimeoutFn    setTimeoutFn;
    getWatchdogBootStatusFn getBootStatusFn;
    writeWatchdogFn         writeWatchdogFn;
} SystemWatchdog_t;


//--------------------------------------------------------------------------------------------
// FOR Unit Tests Only
//--------------------------------------------------------------------------------------------
extern SystemWatchdog_t SysWatchdog;
extern int SysWdMockWdTimeoutS;
extern int SysWdMockWdStatus;

void SystemWatchdog_enableMock(SystemWatchdogRef me, bool enable);



#ifdef __cplusplus
}
#endif

#endif /* SRC_WATCHDOG_SYSTEMWATCHDOG_PRIVATE_H_ */
