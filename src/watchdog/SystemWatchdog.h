/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * SystemWatchdog MODULE
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : SystemWatchdog.h
 * CREATION DATE : Aug 31, 2021
 * TARGET        : embedded Linux
 *
 *********************************************************************************************/
#ifndef SRC_WATCHDOG_SYSTEMWATCHDOG_H_
#define SRC_WATCHDOG_SYSTEMWATCHDOG_H_

/** @ingroup WatchdogMgr
 * @defgroup SystemWatchdog System Watchdog
 * Provides the necessary functions for handling the system watchdog
 */

#include <nmapp/types/nm-types.h>
#include <nmapp/logging/ILog.h>

#include "../FileSystem.h"

#ifdef __cplusplus
extern "C" {
#endif


/*********************************************************************************************
 *        GLOBAL DECLARATIONS
 *********************************************************************************************/

typedef struct __SystemWatchdog * SystemWatchdogRef;


/*********************************************************************************************
 *        GLOBAL FUNCTIONS
 *********************************************************************************************/
//--------------------------------------------------------------------------------------------
/** @brief Create System Watchdog
 * Creates an instance (singleton) of SystemWatchdog
 *
 * @param logRef                A reference to the logger to use
 * @param fileContainer         The file container
 * @param watchdogTimeoutMS     The timeout of the watchdog to configure [ms]
 * @param useWdBypass           false with real watchdog, true to bypass the real watchdog calls (mocking)
 * @return The reference to the created instance (singleton)
 */
SystemWatchdogRef SystemWatchdog_create(ILogRef logRef, FileContainer_t fileContainer, uint32_t watchdogTimeoutMS, bool useWdBypass);
//--------------------------------------------------------------------------------------------
/** @brief Destroy System Watchdog
 * Destroys the instance of SystemWatchdog
 *
 * @param me  A reference to the SystemWatchdog instance
 */
void SystemWatchdog_destroy(SystemWatchdogRef me);
//--------------------------------------------------------------------------------------------
/** @brief Init System Watchdog
 * Initializes the instance of SystemWatchdog
 *
 * @param me  A reference to the SystemWatchdog instance
 */
void SystemWatchdog_init(SystemWatchdogRef me);
//--------------------------------------------------------------------------------------------
/** @brief Feed System Watchdog
 * Feeds the SystemWatchdog
 *
 * @param me  A reference to the SystemWatchdog instance
 */
void SystemWatchdog_feed(SystemWatchdogRef me);





#ifdef __cplusplus
}
#endif

#endif /* SRC_WATCHDOG_SYSTEMWATCHDOG_H_ */
