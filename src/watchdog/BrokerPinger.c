/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * BrokerPinger MODULE
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : BrokerPinger.c
 * CREATION DATE : Aug 30, 2021
 * TARGET        : embedded Linux
 *
 *********************************************************************************************/

#include <stdlib.h>                    // for calloc, free, exit, ...
#include <errno.h>                     // for error numbers
#include <string.h>                    // for strerror
#include <time.h>                      // for time
#include <unistd.h>                    // for read

#include <nmapp/exception/exception.h>
#include <nmapp/sys/assertion.h>
#include <nmapp/conversion/conversion.h>
#include "BrokerPinger_private.h"

/*********************************************************************************************
 *        LOCALS
 *********************************************************************************************/

const uint32_t PING_RESPONSE_READ_DELAY_MS = 100;
enum { PING_BUF_LEN = 32 };


//--------------------------------------------------------------------------------------------
void pingResponseDelayTimerElapsed(void* calleCtx)
{
    BrokerPingerRef me = (BrokerPingerRef)calleCtx;
    assert(me != NULL);
    static char buffer[PING_BUF_LEN] = "";

    ILog_msg(me->logRef, LOG_LEVEL_DBG, "SSF broker ping response timer elapsed");

    EvtLoop_stopTimer(me->pingResonseTimerRef);
    memset(&buffer[0], 0, PING_BUF_LEN);

    IFileOperation_open(me->pingResponse.fileOpItf, me->pingResponse.fileName, "r");
    IFileOperation_readFromFile(me->pingResponse.fileOpItf, &buffer[0], (uint32_t)(PING_BUF_LEN-1), (uint32_t)0);
    IFileOperation_close(me->pingResponse.fileOpItf);

    ILog_msg(me->logRef, LOG_LEVEL_DBG,
             "read from response file %d bytes, trimming newlines and carriage return...", (int)strlen(buffer));
    buffer[strcspn(buffer, "\r\n")] = '\0';      // terminate string at \n or \r

    uint32_t responseLength = (uint32_t)strlen(buffer);
    ILog_msg(me->logRef, LOG_LEVEL_DBG, "broker ping response (len=%d): '%s'", (int)responseLength, buffer);
    if(responseLength == 0)
    {
        ILog_msg(me->logRef, LOG_LEVEL_WARN, "Empty broker ping response received");
        return;
    }
    uint32_t responseValue = str2uint(&buffer[0]);
    bool isValid = (me->pingValue == responseValue);
    snprintf(&buffer[0], (size_t)PING_BUF_LEN, "%s", (isValid) ? PING_STATUS_OK : PING_STATUS_NOK);
    ILog_msg(me->logRef, LOG_LEVEL_DBG, "broker ping status: %s", buffer);
    EvtLoopSyncer_syncEvent(me->resultSyncerRef, SSF_PING_RESPONSE_ID,
                            (const void*)&buffer[0], (uint32_t)strlen(buffer));
}


/*********************************************************************************************
 *        GLOBALS
 *********************************************************************************************/
//--------------------------------------------------------------------------------------------
BrokerPingerRef BrokerPinger_create(EvtLoopRef evtLoopRef, EvtLoopSyncerRef syncerRef, ILogRef logRef,
                                    FileContainer_t pingRequestContainer, FileContainer_t pingResponseContainer)
{
    assert(evtLoopRef != NULL);
    assert(syncerRef != NULL);
    assert(pingRequestContainer.fileOpItf != NULL);
    assert(pingRequestContainer.fileName  != NULL);
    assert(pingResponseContainer.fileOpItf != NULL);
    assert(pingResponseContainer.fileName  != NULL);

    BrokerPingerRef me = (BrokerPingerRef)calloc(1, sizeof(BrokerPingerPrivate_t));
    if(me == NULL)
    {
        Throw(EX_CREATE, "Could not create SSF broker pinger instance");
    }
    me->logRef     = logRef;
    me->evtLoopRef = evtLoopRef;
    me->pingRequest  = pingRequestContainer;
    me->pingResponse = pingResponseContainer;
    me->resultSyncerRef = syncerRef;

    me->responseReadDelayMS = (uint32_t)PING_RESPONSE_READ_DELAY_MS;
    me->pingResonseTimerRef = EvtLoop_createAndRegisterTimer(me->evtLoopRef, &pingResponseDelayTimerElapsed,
                                                             me->responseReadDelayMS, false, me);
    ILog_msg(me->logRef, LOG_LEVEL_DBG, "BrokerPinger successfully created");
    return me;
}
//--------------------------------------------------------------------------------------------
void BrokerPinger_destroy(BrokerPingerRef me)
{
    if(me == NULL)
    {
        return;
    }
    EvtLoop_destroyTimer(me->pingResonseTimerRef);
    free(me);
}
//--------------------------------------------------------------------------------------------
void BrokerPinger_ping(BrokerPingerRef me)
{
    assert(me != NULL);

    char buffer[PING_BUF_LEN] = "";

    time_t currTime = time(NULL);
    if(currTime == ((time_t) -1))
    {
        Throw(EX_READ, "Could not get current time: %s", strerror(errno));
    }
    me->pingValue = (uint32_t)currTime;
    snprintf(&buffer[0], PING_BUF_LEN, "%u", me->pingValue);
    IFileOperation_open(me->pingRequest.fileOpItf, me->pingRequest.fileName, "w");
    IFileOperation_writeToFile(me->pingRequest.fileOpItf, (const char*)buffer, (uint32_t)strlen(buffer));
    IFileOperation_close(me->pingRequest.fileOpItf);
    ILog_msg(me->logRef, LOG_LEVEL_DBG, "Broker ping sent (value=%u/buffer=%s)", (unsigned int)me->pingValue, buffer);
    EvtLoop_startTimer(me->pingResonseTimerRef);
}

