/*********************************************************************************************
 * (c) COPYRIGHT 2020 by NetModule AG, Switzerland.  All rights reserved.
 * -------------------------------------------------------------------------------------------
 *
 * BrokerPinger MODULE
 *
 * -------------------------------------------------------------------------------------------
 *
 * FILENAME      : BrokerPinger.h
 * CREATION DATE : Aug 30, 2021
 * TARGET        : embedded Linux
 *
 *********************************************************************************************/
#ifndef SRC_BROKERPINGER_H_
#define SRC_BROKERPINGER_H_

/** @ingroup SsfManager
 * @defgroup BrokerPinger Broker Pinger
 * Pings the SSF broker and checks its response.
 */

#include <nmapp/types/nm-types.h>
#include <nmapp/logging/ILog.h>
#include <nmapp/event/evt-loop.h>
#include "../EvtLoopSyncer.h"
#include "../FileSystem.h"

#ifdef __cplusplus
extern "C" {
#endif


/*********************************************************************************************
 *        GLOBAL DECLARATIONS
 *********************************************************************************************/

/** Forward declaration of the BrokerPinger object reference **/
typedef struct __BrokerPingerPrivate * BrokerPingerRef;

/** Logger name of the BrokerPinger **/
#define LOG_NAME_BROKER_PINGER  "brokerPinger"

/** Broker ping status = valid/ok **/
#define PING_STATUS_OK      "OK"

/** Broker ping status = invalid/not ok **/
#define PING_STATUS_NOK     "NOK"



/*********************************************************************************************
 *        GLOBAL FUNCTIONS
 *********************************************************************************************/
//--------------------------------------------------------------------------------------------
/** @brief Create Broker Pinger
 * Creates an instance of the BrokerPinger
 *
 * @param evtLoopRef                A reference to the event-loop instance
 * @param syncerRef                 A reference to the event-loop syncer instance
 * @param logRef                    A reference to the logger
 * @param pingRequestContainer      The broker ping request file container
 * @param pingResponseContainer     The broker ping response file container
 * @return The reference to the created instance
 */
BrokerPingerRef BrokerPinger_create(EvtLoopRef evtLoopRef, EvtLoopSyncerRef syncerRef, ILogRef logRef,
                                    FileContainer_t pingRequestContainer, FileContainer_t pingResponseContainer);
//--------------------------------------------------------------------------------------------
/** @brief Destroy Broker Pinger
 * Destroys an instance of the BrokerPinger
 *
 * @param me      A reference to the BrokerPinger instance
 */
void BrokerPinger_destroy(BrokerPingerRef me);
//--------------------------------------------------------------------------------------------
/** @brief Ping Broker
 * Pingsthe broker from an instance of the BrokerPinger
 *
 * @param me      A reference to the BrokerPinger instance
 */
void BrokerPinger_ping(BrokerPingerRef me);


#ifdef __cplusplus
}
#endif

#endif /* SRC_BROKERPINGER_H_ */
